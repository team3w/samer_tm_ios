import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart';
import 'package:location/location.dart';
import 'package:samer_client_flutter/src/CONSTANTS.dart';
import 'package:samer_client_flutter/src/blocs/direction_user_bloc.dart';
import 'package:samer_client_flutter/src/models/direction_entity.dart';
import 'package:samer_client_flutter/src/models/direction_google_model.dart';
import 'package:samer_client_flutter/src/models/envio_entity.dart';
import 'package:samer_client_flutter/src/models/message_entity.dart';
import 'package:samer_client_flutter/src/ui/aditional_info/AditionalInfoWidget.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DireccionWidget extends StatefulWidget {
  @override
  _DireccionWidgetState createState() => _DireccionWidgetState();
}

class _DireccionWidgetState extends State<DireccionWidget> {
  CameraPosition _initialPosition =
      CameraPosition(target: LatLng(26.8206, 30.8025));
  Completer<GoogleMapController> _controller = Completer();
  SharedPreferences prefs;
  static CONSTANTS constants=CONSTANTS();
  String direction = "Click en mapa para buscar...";
  static TextEditingController aliasController = TextEditingController();
  LatLng puntos = LatLng(0,0);
  String alias="";
  var _markers;

  @override
  void initState() {
    super.initState();
    blocDirection.fetchDirectionUser();
    _markers = Set<Marker>();
  }

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
    _currentLocation();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Escoge lugar de envio'),
          centerTitle: false,
        ),
        body: Column(
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              height: MediaQuery.of(context).size.height * 0.55,
              child: Stack(
                children: <Widget>[
                  GoogleMap(
                    markers: _markers,
                    onTap: ((direction) => _mapIdle(direction)),
                    onMapCreated: _onMapCreated,
                    initialCameraPosition: _initialPosition,
                    myLocationEnabled: true,
                    myLocationButtonEnabled: true,
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: SizedBox(
                        width: double.infinity,
                        height: 100,
                        child: Container(
                          color: Colors.black12,
                          child: Column(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 18.0, vertical: 8),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(15.0),
                                  child: Container(
                                    height: 40,
                                    color: Colors.black45,
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Center(
                                          child: Text(
                                        direction,
                                        style: TextStyle(color: Colors.white),
                                        overflow: TextOverflow.ellipsis,
                                      )),
                                    ),
                                  ),
                                ),
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Builder(
                                    builder:(context)=> GestureDetector(
                                      onTap: () {
                                        saveDirection(context);
                                      },
                                      child: Padding(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8.0),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                          child: Container(
                                              width: 150,
                                              height: 35,
                                              color: Colors.green,
                                              child: Center(
                                                  child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    "Guardar  ",
                                                    style: TextStyle(
                                                        color: Colors.white),
                                                  ),
                                                  IconTheme(
                                                    data: IconThemeData(
                                                        color: Colors.white,
                                                        size: 20),
                                                    child: Icon(Icons.add),
                                                  )
                                                ],
                                              ))),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 8.0),
                                    child: GestureDetector(
                                      onTap: () {
                                        goToInfo();
                                      },
                                      child: ClipRRect(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                        child: Container(
                                            width: 150,
                                            height: 35,
                                            color: Colors.green,
                                            child: Center(
                                                child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Text(
                                                  "Continuar  ",
                                                  style: TextStyle(
                                                      color: Colors.white),
                                                ),
                                                IconTheme(
                                                  data: IconThemeData(
                                                      color: Colors.white,
                                                      size: 20),
                                                  child:
                                                      Icon(Icons.arrow_forward),
                                                )
                                              ],
                                            ))),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        )),
                  )
                ],
              ),
            ),
            Expanded(
              child: SizedBox(
                width: double.infinity,
                height: 100,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(
                            top: 18.0, bottom: 10, left: 20.0),
                        child: Text("O elige alguna dirección guardada"),
                      ),
                      Expanded(
                        child: StreamBuilder(
                          stream: blocDirection.getDirectionUser,
                          builder: (context,
                              AsyncSnapshot<DirectionResults> snapshot) {
                            if (snapshot.hasData) {
                              return ListView(
                                children: builCardsDirecction(snapshot.data),
                              );
                            } else if (snapshot.hasError) {
                              return Text(snapshot.error.toString());
                            }
                            return Center(child: CircularProgressIndicator());
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )
          ],
        ));
  }

  void _currentLocation() async {
    final GoogleMapController controller = await _controller.future;
    LocationData currentLocation;
    var location = new Location();
    try {
      currentLocation = await location.getLocation();
    } on Exception {
      currentLocation = null;
    }

    controller.animateCamera(CameraUpdate.newCameraPosition(
      CameraPosition(
        bearing: 0,
        target: LatLng(currentLocation.latitude, currentLocation.longitude),
        zoom: 17.0,
      ),
    ));
  }

  List<Widget> builCardsDirecction(DirectionResults data) {
    List<Widget> lista = [];
    for (int i = 0; i < data.results.length; i++) {
      DireccionEntity direction = data.results[i];
      lista.add(GestureDetector(
        onTap: () => setDirection(direction.pk),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0),
          child: SizedBox(
            width: double.infinity,
            height: 80,
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 18.0, left: 20, bottom: 5),
                    child: Text(
                      direction.name_as,
                      style: TextStyle(fontWeight: FontWeight.w800),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0, right: 10),
                        child: Text(
                          direction.address,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ));
    }
    return lista;
  }

  void goToInfo() {
    /*  Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => AditionalInfo(12)),
    );*/
  }

  void _mapIdle(LatLng direction) async {
    putMarker(direction);
    final GoogleMapController controller = await _controller.future;
    LatLngBounds bounds = await controller.getVisibleRegion();
    double primero =
        (bounds.northeast.latitude + bounds.southwest.latitude) / 2;
    double segundo =
        (bounds.northeast.longitude + bounds.southwest.longitude) / 2;
    Client client = Client();
    final _apiKey = 'AIzaSyCSWR5AKUxKapLhel72V0cS5wb0fmy9SX0';
    var _baseUrl = "https://maps.googleapis.com";

    /* Future<ItemModel> fetchMovieList() async {
    print("entered");
    final response = await client
        .get("http://api.themoviedb.org/3/movie/popular?api_key="+_apiKey);
    print(response.body.toString());
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return ItemModel.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }*/
    puntos=direction;
    String latlng =
        direction.latitude.toString() + "," + direction.longitude.toString();

    final response = await client
        .get("$_baseUrl/maps/api/geocode/json?latlng=$latlng&key=$_apiKey");

    print("$_baseUrl/maps/api/geocode/json?latlng=$latlng&key=$_apiKey");
    if (response.statusCode == 200) {
      DirectionResponseModel direction =
          DirectionResponseModel.fromJson(json.decode(response.body));
      setState(() {
        this.direction = direction.results[0].formatted_address;
      });
    } else {
      throw Exception('Failed to load trailers');
    }

    //bloc.fetchDirection(camera.target.latitude.toString()+","+camera.target.longitude.toString());
    //Fluttertoast.showToast(msg: 'end',toastLength: Toast.LENGTH_SHORT);
  }

  setDirection(int pk_direction) {
    EnvioEntity envioEntity = EnvioEntity();
    envioEntity.pk_direction = pk_direction;

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => AditionalInfo(envioEntity)),
    );
  }

  Future<void> putMarker(LatLng direction) async {
    final GoogleMapController controller = await _controller.future;

    setState(() {
      _markers.clear();
      _markers.add(Marker(
        markerId: MarkerId(direction.toString()),
        position: direction,
        infoWindow: InfoWindow(
          title: 'Direcciòn escogida',
        ),
        icon:
            BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueMagenta),
      ));
    });
  }

  saveDirection(BuildContext context) async{
    prefs = await SharedPreferences.getInstance();
    int id_group =  prefs.getInt('id_group');
    print("tu grupo es :"+id_group.toString());
    if(_markers.toList().length==0){
      Fluttertoast.showToast(msg: 'Debe escoger una dirección en el mapa',toastLength: Toast.LENGTH_SHORT);
    }else if(id_group==1){
      Fluttertoast.showToast(msg: 'Clientes coorporativos no pueden agregar dirección' +id_group.toString(),toastLength: Toast.LENGTH_SHORT);
    }
    else{
      showBottomSheet(
          context: context, builder: (context) => getBottomWidget(context));
    }

  }

  getBottomWidget(BuildContext context) {
    return Container(
      height: 200,
      color: Colors.green,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: <Widget>[
            SizedBox(height: 20,),
            Text("ESCOGE UN ALIAS PARA TU NUEVA DIRECCIÓN",style: TextStyle(color:Colors.white),),
            SizedBox(height: 10,),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal:15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 10,),
                  ClipRRect(
                    borderRadius: BorderRadius.circular(15.0),
                    child: Container(
                      color: Colors.black12,
                      child: Container(
                          child: Padding(
                            padding: const EdgeInsets.only(left: 18.0, right: 18.0, top: 0),
                            child: TextField(
                              controller: aliasController,
                              keyboardType: TextInputType.text,
                              style: TextStyle(fontSize: 17,color: Colors.white),
                            ),
                          )),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: 18,),
            ClipRRect(
                borderRadius: BorderRadius.circular(25.0),
                child: RaisedButton(onPressed: ()=>postDirection(), child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal:10.0),
                  child: Text("GUARDAR",style: TextStyle(color:Colors.white),),
                ),color: Colors.purple,))
          ],
        ),
      ),
    );
  }

  postDirection() async {
    Client client = Client();
    var _api = "api/v1.0/franchise/mobile/affiliates/direction/frequent/";
    var _baseUrl=constants.URL;

    List<double> points= [puntos.latitude,puntos.longitude];
    String token = prefs.getString('token');

    Map<String, dynamic> point={
      'type': "0",
      'coordinates': points,
    };

   Map<String, dynamic> DirectionEnvio={
      'pk': 0,
      'address': direction,
      'principal':false,
      'name_as': aliasController.text,
      'point': point,
    };

   var body= json.encode({"ok":0,"address":direction,"principal":false,"name_as":aliasController.text,"point":{"type":0,"coordinates":points}});


    Map<String, String> headers = {
      'Authorization': "bearer " + token,
      "Content-Type": "application/json"
    };

   print("latitud:"+ puntos.latitude.toString());
   print("longitud:"+ puntos.longitude.toString());
   print("nombre:"+ direction );
   print("alias:"+ aliasController.text);

   print(body);

   final response = await client.post(_baseUrl+_api.toString(),body: body,headers: headers);
    if (response.statusCode == 200) {
      print(response.body);
      print(MessageEntity.fromJson(json.decode(response.body)).toString());
      MessageEntity message= MessageEntity.fromJson(json.decode(response.body));
      print(message.id_location.toString());
      EnvioEntity envioEntity = EnvioEntity();
      envioEntity.pk_direction = message.id_location;
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => AditionalInfo(envioEntity)),
      );


    }else {
      print("${response.statusCode}");
      print("${response.body}");
      Fluttertoast.showToast(msg: 'No se pudo enviar direccion ',toastLength: Toast.LENGTH_SHORT);
      throw Exception('Failed to post direction');

    }

  }
}

Widget buildText(AsyncSnapshot<DirectionResponseModel> snapshot) {
  return Text(snapshot.data.toString());
}
