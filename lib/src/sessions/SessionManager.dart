import 'package:shared_preferences/shared_preferences.dart';

class MyPreferences{
  static final MyPreferences instance = MyPreferences._internal();
  SharedPreferences _sharedPreferences;

  //campos a manejar
  bool isLogin=false;
  String user="";
  String password="";

  MyPreferences._internal() ;

  factory MyPreferences()=> instance;

  Future<SharedPreferences> get preferences async{
    if(_sharedPreferences !=null){
      return _sharedPreferences;
    }else{
      _sharedPreferences = await SharedPreferences.getInstance();
      isLogin=_sharedPreferences.getBool("automatic");
      user=_sharedPreferences.getString("user");
      password=_sharedPreferences.getString("password");
      if(isLogin==null){
        isLogin=false;
        user="";
        password="";
      }
      return _sharedPreferences;
    }

  }

  Future<MyPreferences> init() async{
    await preferences;
    return this;

  }

  Future<bool> commit() async{
    _sharedPreferences.setBool("isLogin", isLogin);
  }
}