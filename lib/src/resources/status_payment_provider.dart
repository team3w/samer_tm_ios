import 'dart:async';
import 'package:http/http.dart' show Client;
import 'package:samer_client_flutter/src/models/status_payment_entity.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';

import '../CONSTANTS.dart';

class StatusPaymentProvider {

  Client client = Client();
  //final _apiKey = '802b2c4b88ea1183e50e6b285a27696e';
  SharedPreferences prefs;

  static CONSTANTS constants=CONSTANTS();

  var _baseUrl=constants.URL;
  var api = "api/v1.0/enterprise/name/payment/";

  Future<StatusPaymentEntity> fetchStatusPayment() async {
    prefs = await SharedPreferences.getInstance();
    String token =  prefs.getString('token');
    print("entered");

    Map<String, String> headers={
      'Authorization': "bearer "+token,
    };

    final response = await client
        .get(_baseUrl+api.toString(),headers: headers);
    print(response.body.toString());
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON

      StatusPaymentEntity.fromJson(json.decode(response.body)).toString();
      return StatusPaymentEntity.fromJson(json.decode(utf8.decode(response.bodyBytes)));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }


}