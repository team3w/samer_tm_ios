import 'package:samer_client_flutter/src/database/database_creator.dart';
import 'package:samer_client_flutter/src/models/producto_envio_pago.dart';



class EnvioFinalEntity{

  List<ProductoEnvioPago> _list_product;
  int     _id_region ;
  int    _type_origin;
  int    _type_payment ;
  String _password_transaction;
  String _observation;
  int     _ruc;
  String _order_day  ;
  double _balance_request   ;
  String _token_transaction;



  EnvioFinalEntity.fromJson(Map<String, dynamic> parsedJson){

    List<ProductoEnvioPago> temp = [];
     _id_region  =parsedJson['_id_region'];
    _type_origin =parsedJson['_type_origin'];
    _type_payment  =parsedJson['_type_payment'];
    _password_transaction=parsedJson['_password_transaction'];
    _observation =parsedJson['_observation'];
     _ruc =parsedJson['_ruc'];
    _order_day   =parsedJson['_order_day'];
    _balance_request =parsedJson['_balance_request'];
    _token_transaction=parsedJson['_token_transaction'];

    for (int i = 0; i < parsedJson['list_product'].length; i++) {
      ProductoEnvioPago result = ProductoEnvioPago(parsedJson['list_product'][i]);
      temp.add(result);
    }
    _list_product = temp;
  }

  List<ProductoEnvioPago> get list_product => _list_product;

  String get token_transaction => _token_transaction;

  double get balance_request => _balance_request;

  String get order_day => _order_day;

  int get ruc => _ruc;

  String get observation => _observation;

  String get password_transaction => _password_transaction;

  int get type_payment => _type_payment;

  int get type_origin => _type_origin;

  int get id_region => _id_region;


}







