

class SubCategoriesResults{
  List<SubCategoryEntity> _data;

  SubCategoriesResults.fromJson(Map<String, dynamic> parsedJson){
    List<SubCategoryEntity> temp = [];
    for (int i = 0; i < parsedJson['results'].length; i++) {
      SubCategoryEntity result = SubCategoryEntity(parsedJson['results'][i]);
      temp.add(result);
    }
    _data = temp;
  }
  List<SubCategoryEntity> get data => _data;
}


class SubCategoryEntity {
  int _pk;
  String _title;
  List<LowCategoryEntity> _children;


  SubCategoryEntity(parsedJson) {
    _pk = parsedJson['pk'];
    _title = parsedJson['title'];
    List<LowCategoryEntity> temp = [];
    for (int i = 0; i < parsedJson['children'].length; i++) {
      LowCategoryEntity result = LowCategoryEntity(parsedJson['children'][i]);
      temp.add(result);
    }
    _children = temp;
  }

  int get pk => _pk;

  String get title => _title;

  List<LowCategoryEntity> get children => _children;


}

class LowCategoryEntity {
  int _pk;
  String _title;

  LowCategoryEntity(parsedJson){
    _pk = parsedJson['pk'];
    _title = parsedJson['title'];
  }

  String get title => _title;

  int get pk => _pk;


}