import 'dart:async';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:samer_client_flutter/src/blocs/status_payment_bloc.dart';
import 'package:samer_client_flutter/src/database/repository_service_product.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'package:samer_client_flutter/src/models/envio_entity.dart';
import 'package:samer_client_flutter/src/models/monto_entity.dart';
import 'package:samer_client_flutter/src/models/producto_envio_pago.dart';
import 'package:samer_client_flutter/src/models/status_payment_entity.dart';
import 'package:samer_client_flutter/src/ui/tarjeta/TarjetaWidget.dart';
import 'package:samer_client_flutter/src/ui/voucher/VoucherWidget.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../CONSTANTS.dart';

class AditionalInfo extends StatefulWidget {
  EnvioEntity envio;
  AditionalInfo(EnvioEntity envioEntity) {
    envio = envioEntity;
  }

  @override
  _AditionalInfoState createState() => _AditionalInfoState(envio);
}

class _AditionalInfoState extends State<AditionalInfo> {
  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  int _radioValue1 = -1;
  int correctScore = 0;

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  static String todayDay = DateTime.now().day.toString();
  static String todayMonth = DateTime.now().month.toString();
  static String todayYear = DateTime.now().year.toString();

  static TextEditingController dayController = TextEditingController(text: "$todayYear-$todayMonth-$todayDay");
  static TextEditingController rucController = TextEditingController();
  static TextEditingController comentaryController = TextEditingController();

  EnvioEntity envioEntity;
  DateTime selectedDate = DateTime.now();

  List<ProductoEnvioPago> list_product = [];
  int id_place;
  Future<List<ProductoEntity>> future;

  Stream stream;
  StreamSubscription sub;
  MontoEntity montos;
  static CONSTANTS constants = CONSTANTS();

  _AditionalInfoState(EnvioEntity envio) {
    envioEntity = envio;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    bloc.fetchStatusPayment();
    getParameters();
    rucController.addListener((){
      print("value: ${rucController.text}");
      if(rucController.text.length==11){
        validarRuc(rucController.text);
      }
    });
  }

   _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      switch (_radioValue1) {
        case 1:
          Fluttertoast.showToast(
              msg: 'Pago con tarjeta', toastLength: Toast.LENGTH_SHORT);
          correctScore++;
          break;
        case 3:
          Fluttertoast.showToast(
              msg: 'Pago con crédito', toastLength: Toast.LENGTH_SHORT);
          break;
        case 5:
          Fluttertoast.showToast(
              msg: 'Pago deposito', toastLength: Toast.LENGTH_SHORT);
          break;
      }
    });
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: selectedDate,
        firstDate: DateTime(2015, 8),
        lastDate: DateTime(2101));
    if (picked != null && picked != selectedDate)
      setState(() {
        selectedDate = picked;
        String Day = picked.day.toString();
        String Month = picked.month.toString();
        String Year = picked.year.toString();
        dayController.text = "$Year-$Month-$Day";
      });
  }

  Widget ruc = Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Text(
          "Ruc (opcional)",
          style: TextStyle(
            color: primary,
            fontSize: 15,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: Container(
          color: Colors.black12,
          child: Container(
              child: Padding(
            padding: const EdgeInsets.only(left: 18.0, right: 18.0, top: 10),
            child: TextField(
              controller: rucController,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly,
                LengthLimitingTextInputFormatter(11)
              ],
              style: TextStyle(fontSize: 18),
            ),
          )),
        ),
      )
    ],
  );

  Widget comentario = Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Text(
          "Comentario (opcional)",
          style: TextStyle(
            color: primary,
            fontSize: 15,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: Container(
          color: Colors.black12,
          child: Padding(
            padding: const EdgeInsets.only(left: 18.0, right: 18.0, top: 10),
            child: TextField(
              controller: comentaryController,
              style: TextStyle(fontSize: 18),
            ),
          ),
        ),
      )
    ],
  );

  Widget fecha = Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Text(
          "Seleccionar dia de entrega",
          style: TextStyle(
            color: primary,
            fontSize: 15,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: Container(
          color: Colors.black12,
          child: Container(
            child: Padding(
              padding: const EdgeInsets.only(left: 18.0, right: 18.0, top: 10),
              child: IgnorePointer(
                child: TextField(
                  controller: dayController,
                  style: TextStyle(fontSize: 18),
                ),
              ),
            ),
          ),
        ),
      )
    ],
  );

  Widget title = Container(
      child: Row(
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(left: 0, top: 0, bottom: 10),
        child: Text(
          "Información adicional",
          style: TextStyle(
            color: primary,
            fontSize: 26,
            fontWeight: FontWeight.bold,
          ),
        ),
      )
    ],
  ));

  Widget tipo_pago() => new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                new RadioListTile(
                  value: 1,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                  title: Text(
                    'Tarjeta',
                    style: new TextStyle(fontSize: 16.0),
                  ),
                ),

              ],
            ),
            Row(
              children: <Widget>[
                new RadioListTile(
                  value: 5,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                  title: Text(
                    'Depósito',
                    style: new TextStyle(
                      fontSize: 16.0,
                    ),
                  ),
                ),

              ],
            ),
            Row(
              children: <Widget>[
                new RadioListTile(
                  value: 3,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                  title: Text(
                    'Crédito',
                    style: new TextStyle(fontSize: 16.0),
                  ),
                ),

              ],
            ),
          ]);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Stack(
            children: <Widget>[
              Container(
                child: Align(
                  child: Builder(
                    builder: (context) => Container(
                          height: 150,
                          child: GestureDetector(
                            onTap: () {
                              sendOrder(context);
                            },
                            child: Container(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Container(
                                  color: Colors.green,
                                  width: MediaQuery.of(context).size.width,
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 0, vertical: 25),
                                    child: Text(
                                      "PAGAR",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20),
                                    ),
                                  ),
                                )
                              ],
                            )),
                          ),
                        ),
                  ),
                  alignment: Alignment.bottomCenter,
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(left: 28.0, right: 28,top: 10,bottom: 80),
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 15,
                    ),
                    title,
                    SizedBox(
                      height: 10,
                    ),
                    GestureDetector(
                        onTap: () => _selectDate(context), child: fecha),
                    SizedBox(
                      height: 10,
                    ),
                    ruc,
                    SizedBox(
                      height: 10,
                    ),
                    comentario,
                    SizedBox(
                      height: 20,
                    ),
                    StreamBuilder(
                      stream: bloc.getStatusPayment,
                      builder: (context,
                          AsyncSnapshot<StatusPaymentEntity> snapshot) {
                        if (snapshot.hasData) {
                          return buildList(snapshot.data);
                        } else if (snapshot.hasError) {
                          return Text(snapshot.error.toString());
                        }
                        return Center(child: CircularProgressIndicator());
                      },
                    ),
                    SizedBox(
                      height: 35,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }




  sendOrder(BuildContext context) {
    //Fluttertoast.showToast(msg: 'pk_direction ' + envioEntity.pk_direction.toString(),toastLength: Toast.LENGTH_SHORT);
    envioEntity.day = dayController.text.toString();
    try {
      envioEntity.ruc = int.parse(rucController.text.toString());
    } catch (error) {
      envioEntity.ruc = 0;
    }
    envioEntity.comentary = comentaryController.text.toString();

    bool passValidation = textFieldValidation();

    if (passValidation) {
      if(_radioValue1==1){
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => TarjetaWidget(envioEntity,this.montos.price_total)),
        );

      }else{
        showBottomSheet(
            context: context, builder: (context) => getBottomWidget(context));
      }

    }

    /* Fluttertoast.showToast(
        msg: 'pk_direction ' +
            envioEntity.pk_direction.toString() +
            'ruc ' +
            envioEntity.ruc.toString() +
            'day ' +
            envioEntity.day.toString() +
            'comentary ' +
            envioEntity.comentary.toString(),
        toastLength: Toast.LENGTH_SHORT); */
  }

  Widget buildList(StatusPaymentEntity data) {
    return new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          data.status_payment == 1 || data.status_payment == 3
              ? new RadioListTile(
                value: 1,
                groupValue: _radioValue1,
                onChanged: _handleRadioValueChange1,
                title: Text(
                  'Tarjeta',
                  style: new TextStyle(fontSize: 16.0),
                ),
              )
              : Container(),
          data.status_payment == 1 || data.status_payment == 3
              ? new RadioListTile(
                value: 5,
                groupValue: _radioValue1,
                onChanged: _handleRadioValueChange1,
                title: Text(
                  'Depósito',
                  style: new TextStyle(
                    fontSize: 16.0,
                  ),
                ),
              )
              : Container(),
          data.status_payment == 2 || data.status_payment == 3
              ? new RadioListTile(
                value: 3,
                groupValue: _radioValue1,
                onChanged: _handleRadioValueChange1,
                title: Text(
                  'Crédito',
                  style: new TextStyle(fontSize: 16.0),
                ),
              )
              : Container()
        ]);
  }

  bool textFieldValidation() {
    if (envioEntity.ruc.toString().length != 11 &&
        envioEntity.ruc.toString().length > 0 &&
        envioEntity.ruc != 0) {
      Fluttertoast.showToast(
          msg: 'RUC debe tener 11 dígitos', toastLength: Toast.LENGTH_SHORT);
      return false;
    }
    if (_radioValue1 == -1) {
      Fluttertoast.showToast(
          msg: 'Debe seleccionar un método de pago',
          toastLength: Toast.LENGTH_SHORT);
      return false;
    }
    return true;
  }

  Widget getBottomWidget(BuildContext context) {
    return Container(
      height: 300,
      color: Colors.white30,
      child: Center(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 0.0),
          child: Container(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              SizedBox(
                height: 40,
              ),
              Text(
                "RESUMEN DE PAGO",
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 17),
              ),
              SizedBox(
                height: 40,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 80.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Compras"),
                    Text("S/." + this.montos.total.toString())
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 80.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Descuento"),
                    Text(
                      "S/." + this.montos.amount_discount.toString(),
                      style: TextStyle(color: Colors.green),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 80.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text("Envio"),
                    Text(
                      "S/." + this.montos.price_delivery.toString(),
                      style: TextStyle(color: Colors.redAccent),
                    )
                  ],
                ),
              ),
              SizedBox(
                height: 45,
              ),
              GestureDetector(
                onTap: () => sendOrderFinal(context),
                child: Container(
                    child: Container(
                  color: Colors.green,
                  width: 900,
                  height: 60,
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 0, vertical: 20),
                    child: Text(
                      "PAGAR",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ),
                )),
              ),
            ],
          )),
        ),
      ),
    );
  }

  void getParameters() async {
    List<ProductoEnvioPago> envio = List();
    List<ProductoEntity> productoEntity =
        await RepositoryServiceTodo.getAllTodos();
    print("cantidad de productos " + productoEntity.length.toString());

    productoEntity.map((producto) {
      envio.add(ProductoEnvioPago.constructor(producto.pk, producto.quantity));
    }).toList();

    print("cantidad de lista " + envio.length.toString());
    envio.toString();
    MontoEntity montoEntity =
        await getPayments(envio, envioEntity.pk_direction);
    setState(() {
      this.montos = montoEntity;
    });
  }

  Future<MontoEntity> getPayments(
      List<ProductoEnvioPago> envio, int pk_direction) async {
    Client client = Client();
    //final _apiKey = '802b2c4b88ea1183e50e6b285a27696e';

    var _baseUrl = constants.URL;
    var api = "api/v1.0/shop/mobile/products/discount/";
    SharedPreferences prefs;
    prefs = await SharedPreferences.getInstance();
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');

    var body = json.encode({"list_product": envio, 'id_places': pk_direction});

    Map<String, String> headers = {
      'Authorization': "bearer " + token,
      "Content-Type": "application/json"
    };

    final response = await client.post(_baseUrl + api.toString(),
        body: body, headers: headers);

    print(response.body);
    print(response.statusCode.toString());
    if (response.statusCode == 200) {
      print(MontoEntity.fromJson(json.decode(response.body)).toString());
      MontoEntity montoEntity =
          MontoEntity.fromJson(json.decode(response.body));
      print(montoEntity.toString());
      return MontoEntity.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      print(body);
      print(response.body);
      throw Exception('Failed to load post');
    }
  }

  sendOrderFinal(BuildContext context) async {
    List<ProductoEnvioPago> envio = List();
    List<ProductoEntity> productoEntity =
        await RepositoryServiceTodo.getAllTodos();
    print("cantidad de productos " + productoEntity.length.toString());

    productoEntity.map((producto) {
      envio.add(ProductoEnvioPago.constructor(producto.pk, producto.quantity));
    }).toList();

    print("cantidad de lista " + envio.length.toString());
    envio.toString();
    MontoEntity montoEntity =
        await sendOrderFinalRequest(envio, envioEntity.pk_direction);
  }

  Future<MontoEntity> sendOrderFinalRequest(
      List<ProductoEnvioPago> envio, int pk_direction) async {
    Client client = Client();
    //final _apiKey = '802b2c4b88ea1183e50e6b285a27696e';

    envioEntity.day = dayController.text.toString();
    try {
      envioEntity.ruc = int.parse(rucController.text.toString());
    } catch (error) {
      envioEntity.ruc = 0;
    }
    envioEntity.comentary = comentaryController.text.toString();

    var _baseUrl = constants.URL;
    var api = "api/v1.0/shop/checkout/processing/";
    SharedPreferences prefs;
    prefs = await SharedPreferences.getInstance();
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');

    if (envioEntity.ruc == null) {
      envioEntity.ruc = 0;
    }

    var body = json.encode({
      "products": envio,
      'id_region': pk_direction,
      'type_origin': 3,
      'type_payment': _radioValue1,
      'password_transaction': "0",
      'observation': envioEntity.comentary,
      'ruc': envioEntity.ruc,
      'balance_request': 0,
      'order_day': envioEntity.day + " 0" + ":" + "0"
    });

    Map<String, String> headers = {
      'Authorization': "bearer " + token,
      "Content-Type": "application/json"
    };

    final response = await client.post(_baseUrl + api.toString(),
        body: body, headers: headers);

    print(response.body);
    print(response.statusCode.toString());
    if (response.statusCode == 201) {

      await RepositoryServiceTodo.deleteAllTodos();
      Fluttertoast.showToast(msg: 'Tu compra se registró correctamente',toastLength: Toast.LENGTH_LONG);
      if (_radioValue1 == 5) {
        Navigator.pushNamedAndRemoveUntil(
            context, '/initial', (Route<dynamic> route) => false);

        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => VoucherWidget(
                  json.decode(response.body)['order_id'].toString())),
        );
      } else {
        Navigator.pushNamedAndRemoveUntil(
            context, '/initial', (Route<dynamic> route) => false);
      }
    } else {
      // If that call was not successful, throw an error.
      print(body);
      print(response.body);
      throw Exception('Failed to load post');
    }
  }

  void validarRuc(String ruc) async{
    Fluttertoast.showToast(msg: 'Vamos a verificar tu ruc',toastLength: Toast.LENGTH_SHORT);
    Client client = Client();

    var _baseUrl = "http://198.58.101.51/";
    var api = "api/v1/ruc/";
    print(_baseUrl + api +ruc+"?token=NRhi1Ahetb");

    final response = await client.get(_baseUrl + api +ruc+"?token=NRhi1Ahetb");

    print(_baseUrl + api +ruc+"?token=NRhi1Ahetb");
    print(response.body);
    print(response.statusCode.toString());
    if (response.statusCode == 200) {
      print(response.body);

    } else {
    // If that call was not successful, throw an error.
      print(response.body);
      throw Exception('Failed to load post');
    }
  }
}
