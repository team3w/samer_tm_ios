import 'dart:convert';

import 'package:flutter_masked_text/flutter_masked_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart';
import 'package:samer_client_flutter/src/database/repository_service_product.dart';
import 'package:samer_client_flutter/src/models/CardEntity.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'package:samer_client_flutter/src/models/card_token_entity.dart';
import 'package:samer_client_flutter/src/models/envio_entity.dart';
import 'package:samer_client_flutter/src/models/message_entity.dart';
import 'package:samer_client_flutter/src/models/monto_entity.dart';
import 'package:samer_client_flutter/src/models/producto_envio_pago.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../CONSTANTS.dart';
import 'package:http/http.dart';

class TarjetaWidget extends StatefulWidget {
  EnvioEntity envio;
  double price;

  TarjetaWidget(EnvioEntity envioEntity, double price_total){
    envio = envioEntity;
    price=price_total;
  }

  @override
  _TarjetaWidgetState createState() => _TarjetaWidgetState(envio,price);
}

class _TarjetaWidgetState extends State<TarjetaWidget> {
  TextEditingController TarjetaController =
      MaskedTextController(mask: '0000-0000-0000-0000');
  TextEditingController FechaController = MaskedTextController(mask: '00/00');
  TextEditingController CVVController = TextEditingController();
  TextEditingController EmailController = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  static CONSTANTS constants=CONSTANTS();
  EnvioEntity envioEntity;
  double dolar=3.698;
  double comision=0;
  double price_total;

  _TarjetaWidgetState(EnvioEntity envio, double price){
    envioEntity = envio;
    price_total=price;
  }

  @override
  void initState() {
    // TODO: implement initState
    getParameters();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Container(
        color: Color.fromRGBO(133, 96, 170, 1),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 48.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                SizedBox(
                  height: 40,
                ),
                TextFormField(
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Campo vacìo';
                    }
                    return null;
                  },
                  style: TextStyle(color: Colors.white, fontSize: 17),
                  controller: TarjetaController,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly,
                    LengthLimitingTextInputFormatter(19)
                  ],
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    labelText: "Tarjeta",
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 0,
                ),
                TextFormField(
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Campo vacìo';
                    }
                    return null;
                  },
                  style: TextStyle(color: Colors.white, fontSize: 17),
                  controller: FechaController,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly,
                    LengthLimitingTextInputFormatter(5)
                  ],
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    labelText: "Vencimiento MM/AA",
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 0,
                ),
                TextFormField(
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Campo vacìo';
                    }
                    return null;
                  },
                  style: TextStyle(color: Colors.white, fontSize: 17),
                  controller: EmailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    labelText: "Email",
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 0,
                ),
                TextFormField(
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'Campo vacìo';
                    }
                    return null;
                  },
                  style: TextStyle(color: Colors.white, fontSize: 17),
                  controller: CVVController,
                  keyboardType: TextInputType.number,
                  inputFormatters: <TextInputFormatter>[
                    WhitelistingTextInputFormatter.digitsOnly,
                    LengthLimitingTextInputFormatter(3)
                  ],
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    labelText: "CVV",
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 28,
                ),
                Container(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15.0),
                    child: Container(
                      color: Colors.white,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 18.0, vertical: 15),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              "RESUMEN",
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 18.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("Compra"),
                                  Builder(builder: (BuildContext){
                                    if(this.price_total==null){
                                      return Text("0");
                                    }else{
                                      return Text(this.price_total.toString());
                                    }
                                  })
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 4,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 18.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text("Pasarela pago"),
                                  Builder(builder: (BuildContext){
                                    if(this.comision==null){
                                      return Text("0");
                                    }else{
                                      return Text(this.comision.toString());
                                    }
                                  }),

                                ],
                              ),
                            ),
                            SizedBox(
                              height: 8,
                            ),
                            Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 18.0),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Text(
                                    "TOTAL",
                                    style: TextStyle(
                                        color: Colors.blue,
                                        fontWeight: FontWeight.bold),
                                  ),
                                  Builder(builder: (BuildContext){
                                    if(this.price_total==null){
                                      return Text("0");
                                    }else{
                                      return Text(
                                        (this.comision+this.price_total).toString(),
                                        style: TextStyle(
                                            color: Colors.blue,
                                            fontWeight: FontWeight.bold),
                                      );
                                    }
                                  }),

                                ],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40,
                ),
                Container(
                  width: 80,
                  child: RaisedButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 10),
                      child: Text("REGISTRARSE"),
                    ),
                    color: Color.fromRGBO(237, 174, 66, 0.9),
                    textColor: Colors.white,
                    onPressed: () {
                      // Validate returns true if the form is valid, otherwise false.
                      if (_formKey.currentState.validate()) {
                        validate(context);
                      }
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      )),
    );
  }

  void validate(BuildContext context) {
    print(TarjetaController.text.replaceAll("-", ""));
    print(FechaController.text.substring(0,2));
    print(FechaController.text.substring(3,5));
    print(CVVController.text.replaceAll("-", ""));
    print(EmailController.text.replaceAll("-", ""));


    CardEntity card = CardEntity(
         TarjetaController.text.replaceAll("-", ""),
        CVVController.text,
        int.parse(FechaController.text.substring(0,2)),
        int.parse(FechaController.text.substring(3,5)),
        EmailController.text
    );

    /*createToken(card: card, apiKey: "pk_live_wuu8sRXLTp1F1Q61").then((CToken token){
      //su token
      print(token.id);
    }).catchError((error){
      try{
        throw error;
      } on CulqiBadRequestException catch(ex){
        print(ex.cause);
      } on CulqiUnknownException catch(ex){
        //codigo de error del servidor
        print(ex.cause);
      }
    });*/

    getToken(card);

  }

  void getToken(CardEntity card) async{

    Client client = Client();

    var _api = "api/v1.0/company/mobile/particular/validate-email/";


    var _baseUrl="https://secure.culqi.com/v2/tokens/";

    Map<String, String> headers={
      "Content-Type": "application/json",
      "Authorization": "Bearer "+ "pk_live_wuu8sRXLTplFlQ6l"
    };

    var registroEnvio=json.encode({
      'card_number': card.card_number,
      'cvv': card.cvv,
      'expiration_month': card.expiration_month,
      'expiration_year': card.expiration_year,
      'email': card.email,
    });

    final response =
    await client.post(_baseUrl.toString(),body: registroEnvio,headers: headers);

    if (response.statusCode == 201) {
      print(response.body);
      CardTokenEntity cardTokenEntity= CardTokenEntity.fromJson(json.decode(response.body));
      enviarToken(context,cardTokenEntity);

    }
    else {
      //Fluttertoast.showToast(msg: 'Error Email ya existe',toastLength: Toast.LENGTH_LONG);
      print("${response.statusCode}");
      print("${response.body}");
      throw Exception('Failed in api validate email in registrer');
    }
  }

  void enviarToken(BuildContext context, CardTokenEntity cardTokenEntity) {

  }

  void getParameters() async {
    List<ProductoEnvioPago> envio = List();
    List<ProductoEntity> productoEntity =
    await RepositoryServiceTodo.getAllTodos();
    print("cantidad de productos " + productoEntity.length.toString());

    productoEntity.map((producto) {
      envio.add(ProductoEnvioPago.constructor(producto.pk, producto.quantity));
    }).toList();

    print("cantidad de lista " + envio.length.toString());
    envio.toString();
    MontoEntity montoEntity =
    await getPayments(envio, envioEntity.pk_direction);
    setState(() {
      this.comision=1.18*(montoEntity.price_total*0.042+0.3*this.dolar);

    });
  }


  Future<MontoEntity> getPayments(
      List<ProductoEnvioPago> envio, int pk_direction) async {
    Client client = Client();
    //final _apiKey = '802b2c4b88ea1183e50e6b285a27696e';

    var _baseUrl = constants.URL;
    var api = "api/v1.0/shop/mobile/products/discount/";
    SharedPreferences prefs;
    prefs = await SharedPreferences.getInstance();
    prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');

    var body = json.encode({"list_product": envio, 'id_places': pk_direction});

    Map<String, String> headers = {
      'Authorization': "bearer " + token,
      "Content-Type": "application/json"
    };

    final response = await client.post(_baseUrl + api.toString(),
        body: body, headers: headers);

    print(response.body);
    print(response.statusCode.toString());
    if (response.statusCode == 200) {
      print(MontoEntity.fromJson(json.decode(response.body)).toString());
      MontoEntity montoEntity =
      MontoEntity.fromJson(json.decode(response.body));
      print(montoEntity.toString());
      return MontoEntity.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      print(body);
      print(response.body);
      throw Exception('Failed to load post');
    }
  }
}
