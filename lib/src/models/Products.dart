import 'package:samer_client_flutter/src/database/database_creator.dart';

class Product {
  int id;
  String name;
  String info;
  bool isDeleted;

  Product(this.id, this.name, this.info, this.isDeleted);

  /*Product.fromJson(Map<String, dynamic> json) {
    this.id = json[DatabaseCreator.id];
    this.name = json[DatabaseCreator.name];
    this.info = json[DatabaseCreator.info];
    this.isDeleted = json[DatabaseCreator.isDeleted] == 1;
  }*/
}
