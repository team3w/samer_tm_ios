

class DirectionResults{
  List<DireccionEntity> _results;

  DirectionResults.fromJson(Map<String, dynamic> parsedJson){
    List<DireccionEntity> temp = [];
    for (int i = 0; i < parsedJson['results'].length; i++) {
      DireccionEntity result = DireccionEntity(parsedJson['results'][i]);
      temp.add(result);
    }
    _results = temp;
  }

  List<DireccionEntity> get results => _results;
}







class  DireccionEntity {
  int     _pk              ;
  String  _address         ;
  String  _name_as         ;
  int  _method_payment  ;
  int     _id_enterprise   ;



  DireccionEntity(parsedJson) {
    //print(parsedJson.toString());
    _pk = parsedJson['pk'];
    _address  = parsedJson['address'];
    _name_as = parsedJson['name_as'];
    _method_payment  = parsedJson['method_payment'];
    _id_enterprise = parsedJson['id_enterprise'];
  }


  int get pk => _pk;
  String get address => _address;
  String get name_as => _name_as;
  int get method_payment => _method_payment;
  int get id_enterprise => _id_enterprise;

  @override
  String toString() {
    return 'DireccionEntity{_pk: $_pk, _address: $_address, _name_as: $_name_as, _method_payment: $_method_payment, _id_enterprise: $_id_enterprise}';
  }


}