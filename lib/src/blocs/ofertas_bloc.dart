
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';

import '../resources/Repository.dart';

class OfertasBloc {
  final _repository = Repository();
  final _productsFetcher = PublishSubject<ProductsResults>();

  Stream<ProductsResults> get allOfertas => _productsFetcher.stream;

  fetchAllOfertas() async {
    if(_isDisposed) {
      return;
    }
    ProductsResults itemModel = await _repository.fetchOfertas();
    _productsFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;
  void dispose() {
    _productsFetcher.close();
    _isDisposed = true;
  }

}

final ofertas_bloc = OfertasBloc();