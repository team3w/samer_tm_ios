import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:samer_client_flutter/src/models/RegistrerEntity.dart';
import 'package:samer_client_flutter/src/models/message_entity.dart';
import 'package:samer_client_flutter/src/ui/phone_auth/PhoneAuthWidget.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../CONSTANTS.dart';

class RegisterWidget extends StatefulWidget {
  @override
  _RegisterWidgetState createState() => _RegisterWidgetState();
}

class _RegisterWidgetState extends State<RegisterWidget> {

  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController nameController = TextEditingController();
  TextEditingController apellidosController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController codeController = TextEditingController();

  static CONSTANTS constants=CONSTANTS();
  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  SharedPreferences prefs;
  String state="INGRESAR";
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
            child: Container(
              color: Color.fromRGBO(133, 96, 170, 1),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 48.0),
                child: Form(
                  key: _formKey,
                  child: ListView(
                    children: <Widget>[
                      SizedBox(
                        height: 40,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Campo vacìo';
                          }
                          return null;
                        },
                        style: TextStyle(color: Colors.white, fontSize: 17),
                        controller: emailController,
                        keyboardType: TextInputType.emailAddress,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          labelText: "Correo",
                          labelStyle: TextStyle(color: Colors.white),
                        ),
                      ),
                      SizedBox(
                        height: 0,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Campo vacìo';
                          }
                          return null;
                        },
                        style: TextStyle(color: Colors.white, fontSize: 17),
                        controller: nameController,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          labelText: "Nombres",
                          labelStyle: TextStyle(color: Colors.white),
                        ),
                      ),
                      SizedBox(
                        height: 0,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Campo vacìo';
                          }
                          return null;
                        },
                        style: TextStyle(color: Colors.white, fontSize: 17),
                        controller: apellidosController,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          labelText: "Apellidos",
                          labelStyle: TextStyle(color: Colors.white),
                        ),
                      ),
                      SizedBox(
                        height: 0,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Campo vacìo';
                          }
                          return null;
                        },
                        style: TextStyle(color: Colors.white, fontSize: 17),
                        controller: passwordController,
                        obscureText: true,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          labelText: "Contraseña",
                          labelStyle: TextStyle(color: Colors.white),
                        ),
                      ),
                      SizedBox(
                        height: 0,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Campo vacìo';
                          }
                          return null;
                        },
                        style: TextStyle(color: Colors.white, fontSize: 17),
                        controller: phoneController,
                        keyboardType: TextInputType.phone,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          labelText: "Telefono",
                          labelStyle: TextStyle(color: Colors.white),
                        ),
                      ),
                      SizedBox(
                        height: 0,
                      ),
                      TextFormField(
                        style: TextStyle(color: Colors.white, fontSize: 17),
                        controller: codeController,
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(
                          enabledBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.white),
                          ),
                          labelText: "Cod. Referencia (opcional)",
                          labelStyle: TextStyle(color: Colors.white),
                        ),
                      ),
                      SizedBox(
                        height: 25,
                      ),
                      Container(width: 80,
                        child: RaisedButton(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(
                                vertical: 20.0, horizontal: 10),
                            child: Text("REGISTRARSE"),
                          ),
                          color: Color.fromRGBO(237, 174, 66, 0.9),
                          textColor: Colors.white,
                          onPressed: () {
                            // Validate returns true if the form is valid, otherwise false.
                            if (_formKey.currentState.validate()) {
                              validate(context);
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              ),
            )),
      ),
    );
  }

  validate(BuildContext context) async{
    RegistrerEntity registrerEntity = RegistrerEntity();
    registrerEntity.first_name = nameController.text;
    registrerEntity.last_name=apellidosController.text;
    registrerEntity.email=emailController.text;
    registrerEntity.phone=num.tryParse(phoneController.text) ;
    registrerEntity.password=passwordController.text;
    registrerEntity.code=codeController.text;


    prefs = await SharedPreferences.getInstance();

    Client client = Client();

    var _api = "api/v1.0/company/mobile/particular/validate-email/";


    var _baseUrl=constants.URL;

    Map<String, String> headers={
      "Content-Type": "application/json"
    };

    var registroEnvio=json.encode({
      'first_name': registrerEntity.first_name,
      'last_name': registrerEntity.last_name,
      'email': registrerEntity.email,
      'phone': registrerEntity.phone,
      'password': registrerEntity.password,
      'code': "0",
    });



    registrerEntity.code="0";

    final response =
        await client.post(_baseUrl+_api.toString(),body: registroEnvio,headers: headers);

    if (response.statusCode == 200) {
      print(MessageEntity.fromJson(json.decode(response.body)).toString());
      MessageEntity messageEntity= MessageEntity.fromJson(json.decode(response.body));
      registrer(context,registrerEntity);

    }
    else {
      Fluttertoast.showToast(msg: 'Error Email ya existe',toastLength: Toast.LENGTH_LONG);
      print("${response.statusCode}");
      print("${response.body}");
      throw Exception('Failed in api validate email in registrer');
    }

  }

  void registrer(BuildContext context,RegistrerEntity registrerEntity) {


    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => PhoneAuthWidget(registrerEntity)),
    );

  }
}
