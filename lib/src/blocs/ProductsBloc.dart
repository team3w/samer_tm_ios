
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';

import '../resources/Repository.dart';

class ProductsBloc {
  final _repository = Repository();
  final _productsFetcher = PublishSubject<ProductsResults>();

  Stream<ProductsResults> get allProducta => _productsFetcher.stream;

  fetchAllProducts() async {
    if(_isDisposed) {
      return;
    }
    ProductsResults itemModel = await _repository.fetchProducts();
    _productsFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;
  void dispose() {
    _productsFetcher.close();
    _isDisposed = true;
  }

}

final bloc = ProductsBloc();