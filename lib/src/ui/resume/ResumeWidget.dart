import 'package:flutter/material.dart';

class ResumeWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        bottomNavigationBar: Container(
          height: 150,
          child: GestureDetector(
            onTap: () => sendOrder(),
            child: Container(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Container(
                  color: Colors.green,
                  width: MediaQuery.of(context).size.width,
                  child: Padding(
                    padding:
                        const EdgeInsets.symmetric(horizontal: 0, vertical: 30),
                    child: Text(
                      "PAGAR  S/. 78,02",
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 18),
                    ),
                  ),
                )
              ],
            )),
          ),
        ),
        body: Container(
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 80.0),
              child: Container(
                  child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Text(
                    "RESUMEN DE PAGO",
                    style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 17),
                  ),
                  SizedBox(
                    height: 40,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[Text("Compras"), Text("S/. 352.30")],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[Text("Descuento"), Text("-S/. 00.00",style: TextStyle(color: Colors.green),)],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[Text("Envio"), Text("S/. 00.00",style: TextStyle(color: Colors.redAccent),)],
                  ),
                  SizedBox(
                    height: 35,
                  ),
                  Text(
                    "Has decidido pagar con tarjeta",
                    style: TextStyle(
                        color: Colors.blueAccent,

                        fontSize: 15),
                  ),
                ],
              )),
            ),
          ),
        ),
      ),
    );
  }

  sendOrder() {}
}
