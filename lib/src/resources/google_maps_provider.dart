import 'dart:async';
import 'package:http/http.dart' show Client;
import 'package:samer_client_flutter/src/models/direction_google_model.dart';
import 'dart:convert';



class GoogleMapsProvider {

  Client client = Client();
  final _apiKey = 'AIzaSyD_yBTzsIy23R9KaFUGI7lvUq2Bd6iMrow';
  var _baseUrl="https://maps.googleapis.com";

 /* Future<ItemModel> fetchMovieList() async {
    print("entered");
    final response = await client
        .get("http://api.themoviedb.org/3/movie/popular?api_key="+_apiKey);
    print(response.body.toString());
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return ItemModel.fromJson(json.decode(response.body));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }*/

  Future<DirectionResponseModel> fetchDirection(String latlng) async {
    final response =
    await client.get("$_baseUrl/maps/api/geocode/json?api_key=$_apiKey&latlng=$latlng");

    if (response.statusCode == 200) {
      return DirectionResponseModel.fromJson(json.decode(response.body));
    } else {
      throw Exception('Failed to load trailers');
    }
  }
}