
class Distributors{

  int     _phone                   ;
  String  _first_name              ;
  String  _identification_document ;
  String  _worker_code             ;
  String  _last_name               ;
  int     _pk                      ;
  String  _avatar                  ;
  String  _id_company              ;

  Distributors(parsedJson) {
    _phone                   = parsedJson['phone'];
    _first_name              = parsedJson['first_name'];
    _identification_document = parsedJson['identification_document'];
    _worker_code             = parsedJson['worker_code'];
    _last_name               = parsedJson['last_name'];
    _pk                      = parsedJson['pk'];
    _avatar                  = parsedJson['avatar'];
    _id_company              = parsedJson['id_company'];

  }

  String get id_company => _id_company;

  String get avatar => _avatar;

  int get pk => _pk;

  String get last_name => _last_name;

  String get worker_code => _worker_code;

  String get identification_document => _identification_document;

  String get first_name => _first_name;

  int get phone => _phone;


}