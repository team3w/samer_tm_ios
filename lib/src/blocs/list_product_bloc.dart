
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'package:samer_client_flutter/src/models/list_response.dart';

import '../resources/Repository.dart';

class ListProductBloc {

  final _repository = Repository();
  final _productsFetcher = PublishSubject<ListResponse>();

  Stream<ListResponse> get allListProduct => _productsFetcher.stream;

  fetchListProducts(int pk) async {
    if(_isDisposed) {
      return;
    }
    ListResponse itemModel = await _repository.fetchListProduct(pk);
    _productsFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;
  void dispose() {
    _productsFetcher.close();
    _isDisposed = true;
  }

}

final list_product_bloc = ListProductBloc();