
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/categoryEntity.dart';

import '../resources/Repository.dart';

class CategoriesBloc {
  final _repository = Repository();
  final _productsFetcher = PublishSubject<CategoriesResults>();

  Stream<CategoriesResults> get allCatergories => _productsFetcher.stream;

  fetchAllCategories() async {
    if(_isDisposed) {
      return;
    }
    CategoriesResults itemModel = await _repository.fetchCategories();
    _productsFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;
  void dispose() {
    _productsFetcher.close();
    _isDisposed = true;
  }

}

final categories_bloc = CategoriesBloc();