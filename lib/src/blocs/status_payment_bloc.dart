
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/status_payment_entity.dart';

import '../resources/Repository.dart';

class StatusPaymentBloc {
  final _repository = Repository();
  final _productsFetcher = PublishSubject<StatusPaymentEntity>();

  Stream<StatusPaymentEntity> get getStatusPayment => _productsFetcher.stream;

  fetchStatusPayment() async {
    if(_isDisposed) {
      return;
    }
    StatusPaymentEntity itemModel = await _repository.fetchStatusPayment();
    _productsFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;
  void dispose() {
    _productsFetcher.close();
    _isDisposed = true;
  }

}

final bloc = StatusPaymentBloc();