
import 'package:samer_client_flutter/src/database/database_creator.dart';

class OrdersResults{
  List<OrderEntity> _results;

  OrdersResults.fromJson(Map<String, dynamic> parsedJson){
    List<OrderEntity> temp = [];
    for (int i = 0; i < parsedJson['results'].length; i++) {
      OrderEntity result = OrderEntity(parsedJson['results'][i]);
      temp.add(result);
    }
    _results = temp;
  }

  List<OrderEntity> get results => _results;
}



class OrderEntity{

 int      _pk;
 String   _order_day;
 String   _timestamp;
 double   _total;
 String   _id_region;
 String   _id_state;
 String   _id_state_color;
 String   _id_payments;
 int      _id_distributors;



 OrderEntity(parsedJson) {
    //print(parsedJson.toString());
    _pk      = parsedJson['pk'];
    _order_day= parsedJson['order_day'];
    _timestamp= parsedJson['timestamp'];
    _total   = parsedJson['total'];
    _id_region= parsedJson['id_region'];
    _id_state= parsedJson['id_state'];

  }

 int get id_distributors => _id_distributors;
 String get id_payments => _id_payments;
 String get id_state_color => _id_state_color;
 String get id_state => _id_state;
 String get id_region => _id_region;
 double get total => _total;
 String get timestamp => _timestamp;
 String get order_day => _order_day;
 int get pk => _pk;


}