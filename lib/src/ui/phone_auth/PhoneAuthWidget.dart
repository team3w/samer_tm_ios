import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:samer_client_flutter/src/models/RegistrerEntity.dart';
import 'package:samer_client_flutter/src/models/user_entity.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../CONSTANTS.dart';

class PhoneAuthWidget extends StatefulWidget {
  RegistrerEntity registrerEntity;

  PhoneAuthWidget(RegistrerEntity registro){
    registrerEntity=registro;
  }


  @override
  _PhoneAuthWidgetState createState() => _PhoneAuthWidgetState(registrerEntity);
}

class _PhoneAuthWidgetState extends State<PhoneAuthWidget> {
  SharedPreferences prefs;
  static CONSTANTS constants=CONSTANTS();

  RegistrerEntity registrerEntity;
  TextEditingController codeController = TextEditingController();

  String status= "Autoregistro en proceso...";
  bool autocomplete=true;

  String actualCode;

  _PhoneAuthWidgetState(RegistrerEntity registrer){
    registrerEntity=registrer;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Future.delayed(Duration.zero,(){
      _verifyPhoneNumber(context);
    });
  }



  /// method to verify phone number and handle phone auth
  _verifyPhoneNumber(BuildContext context) async {
    String phoneNumber = "+51" + registrerEntity.phone.toString();
    final FirebaseAuth _auth = FirebaseAuth.instance;
    await _auth.verifyPhoneNumber(
        phoneNumber: phoneNumber,
        timeout: Duration(seconds: 5),
        verificationCompleted: (authCredential) => _verificationComplete(authCredential, context),
        verificationFailed: (authException) => _verificationFailed(authException, context),
        codeAutoRetrievalTimeout: (verificationId) => _codeAutoRetrievalTimeout(verificationId),
        // called when the SMS code is sent
        codeSent: (verificationId, [code]) => _smsCodeSent(verificationId, [code]));
  }


  /// will get an AuthCredential object that will help with logging into Firebase.
  _verificationComplete(AuthCredential authCredential, BuildContext context) {
    FirebaseAuth.instance.signInWithCredential(authCredential).then((authResult) {
      Fluttertoast.showToast(
          msg: "Success!!! UUID is: " + authResult.user.uid, toastLength: Toast.LENGTH_SHORT);
    login();
    });
  }

  _smsCodeSent(String verificationId, List<int> code) {
    // set the verification code so that we can use it to log the user in
    //_smsVerificationCode = verificationId;
    //codeController.text=code.length.toString();
    this.actualCode = verificationId;

  }

  _verificationFailed(AuthException authException, BuildContext context) {
    Fluttertoast.showToast(
        msg: "Exception!! message:" + authException.message.toString(), toastLength: Toast.LENGTH_SHORT);  }

  _codeAutoRetrievalTimeout(String verificationId) {
    // set the verification code so that we can use it to log the user in
   // _smsVerificationCode = verificationId;
    //codeController.text=code[0].toString();
    this.actualCode = verificationId;

    setState(() {
      this.status="No se pudo autocompletar. Porfavor poner codigo SMS manualmente y dar click en ENVIAR CODIGO";
      this.autocomplete=false;
    });

  }

  void signInWithPhoneNumber() async {
    setState(() {
      this.autocomplete=true;
    });
    var _authCredential = await PhoneAuthProvider.getCredential(
        verificationId: actualCode, smsCode: codeController.text);
    FirebaseAuth.instance.signInWithCredential(_authCredential).then((authResult) {
      Fluttertoast.showToast(
          msg: "Success!!! UUID is: " + authResult.user.uid, toastLength: Toast.LENGTH_SHORT);
          login();
    }
          ).catchError((error){
      Fluttertoast.showToast(
          msg: "SMS incorrecto", toastLength: Toast.LENGTH_SHORT);
      setState(() {
        this.autocomplete=false;
      });
    });
  }


  @override
  Widget build(BuildContext context) {


    return SafeArea(
      child: Scaffold(
        body: Container(
          color: Color.fromRGBO(133, 96, 170, 1),
          child: Center(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 48.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text("Codigo enviado a "+registrerEntity.phone.toString(),style: TextStyle(color: Colors.white,fontSize: 18),),
                SizedBox(
                  height: 15,
                ),
                TextField(
                  style: TextStyle(color: Colors.white, fontSize: 17),
                  controller: codeController,

                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    labelText: "Colocar SMS codigo",

                    labelStyle: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Center(child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 20,
                    ),
                    Text(status,style: TextStyle(color: Colors.white,fontSize: 16),),
                    SizedBox(
                      height: 20,
                    ),
                    autocomplete? CircularProgressIndicator():
                    RaisedButton(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                            vertical: 15.0, horizontal: 10),
                        child: Text("ENVIAR CODIGO"),
                      ),
                      color: Color.fromRGBO(237, 174, 66, 0.9),
                      textColor: Colors.white,
                      onPressed: ()=> signInWithPhoneNumber(),
                    ),

                  ],
                ))
              ],
            ),
            ),
          ),

        ),
      ),
    );
  }

  void login() async{


    prefs = await SharedPreferences.getInstance();

    Client client = Client();

    var _api = "api/v1.0/company/mobile/particular/get/";


    var _baseUrl=constants.URL;

    Map<String, String> headers={
      "Content-Type": "application/json"
    };

    var registroEnvio=json.encode({
      'first_name': registrerEntity.first_name,
      'last_name': registrerEntity.last_name,
      'email': registrerEntity.email,
      'phone': registrerEntity.phone,
      'password': registrerEntity.password,
      'code': "0",
    });

    print(registroEnvio);

    registrerEntity.code="0";

    final response =
    await client.post(_baseUrl+_api.toString(),body: registroEnvio,headers: headers);

    if (response.statusCode == 200) {
      print(UserEntity.fromJson(json.decode(response.body)).toString());
      UserEntity userEntity= UserEntity.fromJson(json.decode(response.body));
      await prefs.setString('token', userEntity.token);
      await prefs.setString('user_name', userEntity.first_name);
      await prefs.setString('user_email', userEntity.email);
      await prefs.setString('user_avatar', userEntity.avatar);
      await prefs.setInt('id_group', userEntity.id_group);

      await prefs.setBool('isLogin', true);

      print("tu id group es "+userEntity.id_group.toString());
      Navigator.pushNamedAndRemoveUntil(
          context, '/initial', (Route<dynamic> route) => false);

      //return UserEntity.fromJson(json.decode(response.body));

    }
    else {
      Fluttertoast.showToast(msg: 'Error al conectarse con el servidor',toastLength: Toast.LENGTH_SHORT);
      print("${response.statusCode}");
      print("${response.body}");
      throw Exception('Failed to login after register');
    }

  }


}
