import 'dart:async';
import 'package:http/http.dart' show Client;
import 'package:samer_client_flutter/src/models/sub_category_entity.dart';
import 'dart:convert';

import '../CONSTANTS.dart';

class SubCategoriesApiProvider {
  Client client = Client();

  static CONSTANTS constants=CONSTANTS();

  var _baseUrl=constants.URL;
  var api = "api/v1.0/shop/mobile/category/ios/";

  Future<SubCategoriesResults> fetchSubCategories(int pk) async {
    print("entered categories");
    final response = await client.get(_baseUrl+api+pk.toString()+"/");
    print(response.body.toString());
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return SubCategoriesResults.fromJson(json.decode(utf8.decode(response.bodyBytes)));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }


}