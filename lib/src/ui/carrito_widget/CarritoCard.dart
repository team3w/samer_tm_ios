import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/database/repository_service_product.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';

class CardCarrito extends StatefulWidget {


  ProductoEntity product;

  CardCarrito(this.product);

  @override
  _CardCarritoState createState() => _CardCarritoState(product);
}

class _CardCarritoState extends State<CardCarrito> {

  ProductoEntity product;
  int cantidad=0;

  _CardCarritoState(ProductoEntity product){
    this.product=product;
    getQuantity();

  }

  void getQuantity() async{

    int cantidad=  await RepositoryServiceTodo.ProductCount(product.pk);

    setState(() {
      this.cantidad=cantidad;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Container(
      child: cantidad>0?Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                SizedBox(
                  height: 8,
                ),
                Container(
                  //  color: Colors.green,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(left: 20.0, top: 10),
                        child: Image.network(
                          product.image,
                          height: 80,
                          width: 80,
                          fit: BoxFit.fitWidth,
                        ),
                      ),
                      Flexible(
                        child: Container(
                          //color: Colors.amberAccent,
                          child: Padding(
                            padding:
                            const EdgeInsets.only(left: 10, top: 8, right: 22),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: <Widget>[
                                IconButton(icon:Icon(Icons.delete),onPressed: ()=>deleteItem(product.pk,product.title_products),),
                                SizedBox(
                                  height: 0,
                                ),
                                Text(
                                  product.title_products,
                                  style:
                                  TextStyle(fontSize: 12, color: Colors.black),
                                  overflow: TextOverflow.ellipsis,
                                  maxLines: 1,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom:20,right: 8),
                  child: Container(
                    //color: Colors.amber,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top:4.0),
                          child: Text(cantidad.toString()),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal:8.0),
                          child: GestureDetector(onTap: ()=>quitarProducto(product),
                            //agregarBD(productoEntity),
                              child: IconTheme(
                                data: IconThemeData(size: 26, color: Colors.redAccent),
                                child: Icon(Icons.remove_circle),
                              )),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal:8.0,vertical: 4),
                          child: Text(
                              "S/." +
                                  (product.base_price * product.quantity)
                                      .toString(),
                              style: TextStyle(
                                  color: Colors.purple,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 16)),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(horizontal:8.0),
                          child: GestureDetector(onTap: ()=>agregarProducto(product),
                              //agregarBD(productoEntity),
                              child: IconTheme(
                                data: IconThemeData(size: 26, color: Colors.amber),
                                child: Icon(Icons.add_circle),
                              )),
                        )
                      ],
                    ),
                  ),
                )
              ],
            )),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 5,
        margin: EdgeInsets.only(left: 10, right: 10,top: 15),
      ):Container(),
    );
  }

  agregarProducto(ProductoEntity product) async{

    int quantity;
    quantity= await RepositoryServiceTodo.updateTodo(product) ;
    print("cantidad aumentada a "+quantity.toString());

    setState(() {
      cantidad=cantidad+1;
    });

  }

  quitarProducto(ProductoEntity product) async{

    int quantity;
    if(cantidad>1){
      quantity= await RepositoryServiceTodo.downgradeTodo(product) ;
      print("cantidad disminuida a "+quantity.toString());

      setState(() {
        cantidad=cantidad-1;
      });
    }


  }

  deleteItem(int pk,String name) async{
    //Fluttertoast.showToast(msg: 'Se eliminarà '+name,toastLength: Toast.LENGTH_SHORT);
    await RepositoryServiceTodo.deleteProduct(pk);
    setState(() {
      cantidad=0;
    });
  }
}
