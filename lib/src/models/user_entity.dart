
class UserEntity {
 String _token;
 String _last_name;
 String _email;
 String _username;
 String _avatar;
 String _first_name;
 int    _id_group;
 int    _phone;
 bool   _administrator;
 bool   _isClientFranchies;
 int    _identification_document;

  UserEntity.fromJson(Map<String, dynamic> parsedJson) {


     _token= parsedJson['token'];
     _last_name = parsedJson['last_name'];
     _email  = parsedJson['email'];
     _username = parsedJson['username'];
     _avatar  = parsedJson['avatar'];
     _first_name= parsedJson['first_name'];
     _id_group = parsedJson['id_group'];
     _phone = parsedJson['phone'];
     _administrator = parsedJson['administrator'];
     _isClientFranchies = parsedJson['isClientFranchies'];
     _identification_document = parsedJson['identification_document'];

  }


  String get token=> _token;
  String get last_name=> _last_name;
  String get email=> _email;
  String get username=> _username;
  String get avatar=> _avatar;
  String get first_name=> _first_name;
  int get id_group => _id_group;
  int get phone => _phone;
  bool get administrator => _administrator;
  bool get isClientFranchies => _isClientFranchies;
  int get identification_document => _identification_document;

 @override
 String toString() {
   return 'UserEntity{_token: $_token, _last_name: $_last_name, _email: $_email, _username: $_username, _avatar: $_avatar, _first_name: $_first_name, _id_group: $_id_group, _phone: $_phone, _administrator: $_administrator, _isClientFranchies: $_isClientFranchies, _identification_document: $_identification_document}';
 }


}