
class RegistrerEntity{
  String _first_name;
  String _last_name;
  String _email;
  int _phone;
  String _password;
  String _code;

  String get first_name => _first_name;

  set first_name(String value) {
    _first_name = value;
  }

  String get last_name => _last_name;

  String get code => _code;

  set code(String value) {
    _code = value;
  }

  String get password => _password;

  set password(String value) {
    _password = value;
  }

  int get phone => _phone;

  set phone(int value) {
    _phone = value;
  }

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  set last_name(String value) {
    _last_name = value;
  }


}