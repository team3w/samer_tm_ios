import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/blocs/order_detail_bloc.dart';
import 'package:samer_client_flutter/src/models/DetalleResponseEntity.dart';

class OrderDetailWidget extends StatefulWidget {
  int pk;
  OrderDetailWidget(int pk) {
    this.pk = pk;
  }

  @override
  _OrderDetailWidgetState createState() => _OrderDetailWidgetState(pk);
}

class _OrderDetailWidgetState extends State<OrderDetailWidget> {
  int pk;
  _OrderDetailWidgetState(int pk) {
    this.pk = pk;
  }

  @override
  void initState() {
    super.initState();
    blocOrdersDetail.fetchOrdersDetail(pk);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Pedido N⁰ " + pk.toString()),
        ),
        body: Container(
          child: StreamBuilder(
            stream: blocOrdersDetail.getOrdersDetail,
            builder: (context, AsyncSnapshot<DetalleResponseEntity> snapshot) {
              if (snapshot.hasData) {
                return makeView(snapshot.data);
              } else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }
              return Center(child: CircularProgressIndicator());
            },
          ),
        ),
      ),
    );
  }

  Widget makeView(DetalleResponseEntity data) {
    String distributor_name = data.distributor.first_name;
    String distributor_last = data.distributor.last_name;
    DateTime date= DateTime.parse(data.order_day);
    return Container(
      child: Column(
        children: <Widget>[
          Container(
            color: Colors.deepPurple,
            child: SizedBox(
              height: 75,
              child: Padding(
                padding: const EdgeInsets.symmetric(vertical:8.0,horizontal: 25),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      "DETALLES",
                      style: TextStyle(color: Colors.white),
                    ),
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          data.id_state.name,
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: Colors.green,
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                    )
                  ],
                ),
              ),
            ),
          ),
          SizedBox(height: 10,),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:10.0,vertical: 2),
            child: Card(
              child: Container(
                child: SizedBox(height: 100,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: 60,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical:30.0),
                          child: Icon(Icons.info_outline),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical:18.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text("Repartidor"),
                            Text("$distributor_name ",style: TextStyle(fontWeight: FontWeight.bold),),
                            Text("Telef. " + data.distributor.phone.toString())
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:8.0,vertical: 2),
            child: Card(
              child: Container(
                child: SizedBox(height: 100,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(width: 60,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical:30.0),
                          child: Icon(Icons.map),
                        ),
                      ),
                      Flexible(
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.symmetric(vertical:18.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Text("Dirección"),
                                Text(data.localization_name,style: TextStyle(fontWeight: FontWeight.bold),),
                                Flexible(child: Container(child: Padding(
                                  padding: const EdgeInsets.only(right: 10),
                                  child: Text(data.localization ,overflow: TextOverflow.ellipsis,),
                                )))
                              ],
                            ),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:8.0,vertical: 2),
            child: Card(
              child: Container(
                child: SizedBox(height: 100,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(
                        width: 60,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical:30.0),
                          child: Icon(Icons.date_range),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical:18.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text("Fecha"),
                            Text(date.day.toString()+"/"+date.month.toString()+"/"+date.year.toString(),style: TextStyle(fontWeight: FontWeight.bold),),
                            Text(date.hour.toString()+":"+date.minute.toString())
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal:8.0,vertical: 2),
            child: Card(
              child: Container(
                child: SizedBox(height: 100,
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(width: 60,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical:30.0),
                          child: Icon(Icons.attach_money),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(vertical:18.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text("Total"),
                            Text("S/."+data.total.toString(),style: TextStyle(fontWeight: FontWeight.bold),),
                            Text("Pago " )
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
