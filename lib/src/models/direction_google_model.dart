class DirectionResponseModel {

  List<_Direcion> _results = [];

  DirectionResponseModel.fromJson(Map<String, dynamic> parsedJson) {

    List<_Direcion> temp = [];
    for (int i = 0; i < parsedJson['results'].length; i++) {
      _Direcion result = _Direcion(parsedJson['results'][i]);
      temp.add(result);
    }
    _results = temp;
  }

  List<_Direcion> get results => _results;

  @override
  String toString() {
    return 'DirectionResponseModel{_results: $_results}';
  }


}


class _Direcion {

  String _formatted_address;


  _Direcion(result) {
    _formatted_address = result['formatted_address'];

  }

  String get formatted_address => _formatted_address;

}