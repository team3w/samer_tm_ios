
class MontoEntity{

  double _total;
  double _discount;
  double _amount_discount;
  double _price_delivery;
  bool _shipping;
  double _price_total;

  MontoEntity.fromJson(Map<String, dynamic> parsedJson){
    _total = parsedJson['total'];
    _discount = parsedJson['discount'];
    _amount_discount = parsedJson['amount_discount'];
    _price_delivery = parsedJson['price_delivery'];
    _shipping = parsedJson['shipping'];
    _price_total = parsedJson['price_total'];
  }

  bool get shipping => _shipping;

  double get price_delivery => _price_delivery;

  double get amount_discount => _amount_discount;

  double get discount => _discount;

  double get total => _total;

  double get price_total => _price_total;


  set price_total(double value) {
    _price_total = value;
  }

  @override
  String toString() {
    return 'MontoEntity{_total: $_total, _discount: $_discount, _amount_discount: $_amount_discount, _price_delivery: $_price_delivery, _shipping: $_shipping}';
  }


}