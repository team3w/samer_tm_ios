import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/database/repository_service_product.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'package:samer_client_flutter/src/icons/add_icons.dart';
import 'package:samer_client_flutter/src/ui/producto_detalle/ProductDetailWidget.dart';

class CardPrincipal extends StatefulWidget {
  ProductoEntity productoEntitiy;
  int cantidad;

  CardPrincipal(this.productoEntitiy);

  @override
  _CardPrincipalState createState() => _CardPrincipalState(productoEntitiy);
}



class _CardPrincipalState extends State<CardPrincipal> {
  ProductoEntity productoEntity;
  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  static Color accent= Color.fromRGBO(255, 189, 25,1);
  bool enCarrito;
  int cantidad=0;

  _CardPrincipalState(ProductoEntity productoEntitiy){
    this.productoEntity=productoEntitiy;
    getQuantity();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 550,
      child: Card(
        clipBehavior: Clip.antiAliasWithSaveLayer,
        child: Container(
            height: 550,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                SizedBox(
                  height: 10,
                ),
                GestureDetector(onTap: ()=>goToDetail(context,productoEntity.pk),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 20.0, vertical: 10),
                    child: Image.network(
                      productoEntity.image,
                      height: 100,
                      fit: BoxFit.fitWidth,
                    ),
                  ),
                ),
                GestureDetector(onTap: ()=>goToDetail(context,productoEntity.pk),
                  child: Container(
                    child: Padding(
                      padding: const EdgeInsets.only(left: 12, top: 4, right: 12),
                      child: Center(
                          child: Text(
                            productoEntity.title_products,
                            style: TextStyle(fontSize: 12, color: Colors.black),
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          )),
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(
                      left: 15, top: 13, right: 15, bottom: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text("S/." + productoEntity.base_price.toString(),
                          style: TextStyle(
                              color: primary,
                              fontWeight: FontWeight.bold,
                              fontSize: 16)),

                      Text(cantidad.toString()),
                      GestureDetector(onTap: ()=>agregarBD(productoEntity),
                        child: buildItem(),
                      )
                    ],
                  ),
                )
              ],
            )),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        elevation: 5,
        margin: EdgeInsets.only(left: 10, right: 10, bottom: 15),
      ),
    );
  }

  void agregarBD(ProductoEntity productoEntity) async{

    int cantidad=  await RepositoryServiceTodo.ProductCount(productoEntity.pk);
    int quantity;

    if(cantidad>0){
      quantity= await RepositoryServiceTodo.updateTodo(productoEntity) ;
      print("cantidad aumentada a "+quantity.toString());

    }else{
      await RepositoryServiceTodo.addTodo(productoEntity) ;
      print("agregado nuevo producto");
      quantity=1;
    }

    setState(() {
      this.cantidad=quantity;
    });



  }

  Widget buildItem() {
    if(cantidad<=0) {
      return IconTheme(
        data: IconThemeData(
            size: 26,
            color: accent),
        child: Icon(Add.plus_circled),
      );
    }else{
      return IconTheme(
        data: IconThemeData(
            size: 26,
            color: Colors.green),
        child: Icon(Add.plus_circled),
      );

    }
  }

  void getQuantity() async{

    int cantidad=  await RepositoryServiceTodo.ProductCount(productoEntity.pk);
    if(cantidad==null){
      cantidad=0;
    }
    setState(() {
      this.cantidad=cantidad;
    });
  }

  goToDetail(BuildContext context, int pk) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ProductDetailWidget(pk)),
    );
  }

}


