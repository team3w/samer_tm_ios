import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/CONSTANTS.dart';
import 'package:samer_client_flutter/src/database/repository_service_product.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'package:samer_client_flutter/src/ui/principal_widget/CardPrincpal.dart';


class SearchWidget extends StatefulWidget {

  @override
  _SearchWidgetState createState() => _SearchWidgetState();
}


class _SearchWidgetState extends State<SearchWidget> {
  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  static TextEditingController searchController = TextEditingController();
  String searchtext = "";
  final _productsFetcher = PublishSubject<String>();
  ProductsResults productsResults;
  int estado=0;
  Client client = Client();
  static CONSTANTS constants=CONSTANTS();


  @override
  void initState() {

   /* searchController.addListener(() {
      if (searchController.text.length > 3) {
        searchProduct(searchController.text);
      }
    });*/

  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(
            body: Container(
      child: Center(
          child: Column(
        children: <Widget>[
          SizedBox(
            height: 30,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    ClipRRect(
                      borderRadius: BorderRadius.circular(15.0),
                      child: SizedBox(
                        width: MediaQuery.of(context).size.width * 0.7,
                        child: Container(
                          color: Colors.black12,
                          child: Container(
                              child: Padding(
                            padding: const EdgeInsets.only(
                                left: 18.0, right: 50.0, top: 10),
                            child: TextField(
                              decoration: InputDecoration(
                                  hintText: "Buscar producto ..."),
                              controller: searchController,
                              keyboardType: TextInputType.text,
                              style: TextStyle(fontSize: 18),
                            ),
                          )),
                        ),
                      ),
                    ),
                    SizedBox(
                        width: 55,height: 55,
                        child: ClipRRect(borderRadius: BorderRadius.circular(15),
                          child: RaisedButton(color: primary,
                            child: Icon(Icons.search ,color: Colors.white,),
                            onPressed: () {searchProduct(searchController.text);},
                          ),
                        )),
                  ],
                ),
                SizedBox(
                  height: 22,
                  //MediaQuery.of(context).size.width * 0.1,
                ),
              ],
            ),
          ),
          FutureBuilder<ProductsResults>(builder: (BuildContext context, AsyncSnapshot<ProductsResults> snapshot) {
            if(estado==0){
              return Container(child: Center(child: Text("Buscar"),),);
            }else if(estado==1){
              return CircularProgressIndicator();
            }else{
              return BuildLista();
            }
          },)
        ],
      )),
    )));
  }

  Future<void> searchProduct(String search) async {
    setState(() {
      estado=1;
    });

    var _baseUrl=constants.URL;
    var api = "api/v1.0/shop/mobile/products/search?page=1&search="+search;
    print("entered");
    final response = await client.get(_baseUrl+api);
    print(response.body.toString());
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      setState(() {
        estado=2;
        productsResults=ProductsResults.fromJson(json.decode(utf8.decode(response.bodyBytes)));
      });
      return ProductsResults.fromJson(json.decode(utf8.decode(response.bodyBytes)));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }



  }

  Widget BuildLista() {
    return Expanded(
      child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10.0,
          ),
          child: GridView.builder(
              itemCount: productsResults.results.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 2, childAspectRatio: 0.72),
              itemBuilder: (BuildContext context, int index) {
                return  buildCard(productsResults.results[index]);
                //onTap: () => openDetailPage(snapshot.data, index),
              })

      ),
    );
  }


  buildCard(ProductoEntity productoEntity) {
    Future<int> cantidad=  RepositoryServiceTodo.ProductCount(productoEntity.pk);
    return FutureBuilder<int>(future: cantidad,builder:(context,snapshot){

      return CardPrincipal(productoEntity);

    });
  }
}
