
import 'Distributors.dart';

class DetalleResponseEntity{

  int _pk                      ;
  String _order_day            ;
  double _subtotal             ;
  double _total                ;
  bool _cash                 ;
  String _id_payments          ;
  String _timestamp            ;
  int _type_payment         ;
  String _localization         ;
  String _localization_name    ;
  String _url_invoice          ;
  StateEntity _id_state        ;
  DirectionEntity _direction   ;
  Distributors _distributor    ;
  bool _is_distributor         ;

  DetalleResponseEntity.fromJson(Map<String, dynamic> parsedJson){
    
     _pk                = parsedJson['pk'];
     _order_day         = parsedJson['order_day'];
     _subtotal          = parsedJson['subtotal'];
     _total             = parsedJson['total'];
     _cash              = parsedJson['cash'];
     _id_payments       = parsedJson['id_payments'];
     _timestamp         = parsedJson['timestamp'];
     _type_payment      = parsedJson['type_payment'];
     _localization      = parsedJson['localization'];
     _localization_name = parsedJson['localization_name'];
     _url_invoice       = parsedJson['url_invoice'];
     _id_state          = StateEntity(parsedJson['id_state']);
     _direction         = DirectionEntity(parsedJson['direction']);
     _distributor       = Distributors(parsedJson['distributor']);
     _is_distributor    = parsedJson['is_distributor'];
   }

  bool get is_distributor => _is_distributor;

  Distributors get distributor => _distributor;

  DirectionEntity get direction => _direction;

  StateEntity get id_state => _id_state;

  String get url_invoice => _url_invoice;

  String get localization_name => _localization_name;

  String get localization => _localization;

  int get type_payment => _type_payment;

  String get timestamp => _timestamp;

  String get id_payments => _id_payments;

  bool get cash => _cash;

  double get total => _total;

  double get subtotal => _subtotal;

  String get order_day => _order_day;

  int get pk => _pk;


}


class StateEntity{
  int _pk            ;
  String _name       ;
  String _color_hexa ;

  StateEntity(parsedJson) {
    _pk = parsedJson['pk'];
    _name = parsedJson['name'];
    _color_hexa = parsedJson['color_hexa'];
  }

  String get color_hexa => _color_hexa;

  String get name => _name;

  int get pk => _pk;


}

class DirectionEntity{
  Point _point      ;


  DirectionEntity(parsedJson) {
    _point = Point(parsedJson['point']);
  }

  Point get point => _point;


}


class Point{
  List<double> _coordinates=[];
  String _type;

  Point(parsedJson) {

    List<double> temp = [];
    for (int i = 0; i < parsedJson['coordinates'].length; i++) {
      double result = parsedJson['coordinates'][i];
      temp.add(result);
    }
    _coordinates = temp;
    _type = parsedJson['type'];

  }

  String get type => _type;

  List<double> get coordinates => _coordinates;


}