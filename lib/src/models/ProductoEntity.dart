
import 'package:samer_client_flutter/src/database/database_creator.dart';

class ProductsResults{
  List<ProductoEntity> _results;

  ProductsResults.fromJson(Map<String, dynamic> parsedJson){
    List<ProductoEntity> temp = [];
    for (int i = 0; i < parsedJson['results'].length; i++) {
      ProductoEntity result = ProductoEntity(parsedJson['results'][i]);
      temp.add(result);
    }
    _results = temp;
  }

  List<ProductoEntity> get results => _results;
}



class ProductoEntity{
  int _pk;
  String _title_products;
  String _id_brand;
  double _base_price;
  String _image;
  String _id_unit;
  int _quantity;


  ProductoEntity.constructor(this._pk, this._title_products, this._id_brand,
      this._base_price, this._image, this._id_unit);

  ProductoEntity.fromJson(Map<String, dynamic> json) {
    this._pk = json[DatabaseCreator.pk];
    this._title_products = json[DatabaseCreator.title_products];
    this._id_brand = json[DatabaseCreator.id_brand];
    this._base_price = json[DatabaseCreator.base_price];
    this._image = json[DatabaseCreator.image];
    this._id_unit = json[DatabaseCreator.id_unit];
    this._quantity=json[DatabaseCreator.quantity];

  }

  ProductoEntity(parsedJson) {
    //print(parsedJson.toString());
    _pk = parsedJson['pk'];
    _title_products = parsedJson['title_products'];
    _id_brand = parsedJson['id_brand'];
    _base_price = parsedJson['base_price'];
    _image = parsedJson['image'];
    _id_unit = parsedJson['id_unit'];
    _quantity=parsedJson['quantity'];

  }

  int get pk => _pk;
  String get title_products => _title_products;
  String get id_brand       => _id_brand      ;
  double get base_price     => _base_price    ;
  String get image          => _image         ;
  String get id_unit        => _id_unit       ;
  int get quantity => _quantity;


}