
import 'ProductoEntity.dart';

class ListResponse{
  List<ProductoEntity> _products;
  int _count;

  ListResponse.fromJson(Map<String, dynamic> parsedJson){
    List<ProductoEntity> temp = [];
    for (int i = 0; i < parsedJson['products'].length; i++) {
      ProductoEntity result = ProductoEntity(parsedJson['products'][i]);
      temp.add(result);
    }
    _products = temp;
    _count=parsedJson['count'];
  }

  int get count => _count;

  List<ProductoEntity> get products => _products;


}


