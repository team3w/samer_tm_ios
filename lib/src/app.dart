import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:samer_client_flutter/src/ui/bottom_navigation_bar/myBottomNav.dart';
import 'package:samer_client_flutter/src/ui/carrito_widget/CarritoWidget.dart';
import 'package:samer_client_flutter/src/ui/categories/CategoriesWdget.dart';
import 'package:samer_client_flutter/src/ui/favoritos/FavoritosWidget.dart';
import 'package:samer_client_flutter/src/ui/historial.dart';
import 'package:samer_client_flutter/src/ui/login/LoginWidget.dart';
import 'package:samer_client_flutter/src/ui/ofertas/OfertasWidget.dart';
import 'package:samer_client_flutter/src/ui/order/OrderWidget.dart';
import 'package:samer_client_flutter/src/ui/principal_widget/PrincipalWidget.dart';
import 'package:samer_client_flutter/src/blocs/BottomAppBarBloc.dart';
import 'package:samer_client_flutter/src/ui/search/SearchWidget.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'icons/menu_icons.dart';
import 'icons/search_icons.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
        //statusBarColor:  Color.fromRGBO(54, 4, 69, 1),
        //systemNavigationBarColor:  Color.fromRGBO(54, 4, 69, 0.8),
        // statusBarColor:  Color.fromRGBO(54, 4, 69, 1),
        systemNavigationBarColor: Colors.black87,
        statusBarIconBrightness: Brightness.light,
        systemNavigationBarIconBrightness: Brightness.light));
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.deepPurple,
        accentColor: Colors.deepPurple,
        //brightness: Brightness.dark,
      ),
      routes: {
        // When navigating to the "/" route, build the FirstScreen widget.

        // When navigating to the "/second" route, build the SecondScreen widget.
        '/initial': (context) => Home(),
      },
      home: Home(),
    );
  }
}

class Home extends StatefulWidget {

  static Color background = Color.fromRGBO(245, 245, 245, 0.98);
  static Color accent = Color.fromRGBO(255, 189, 25, 1);

  SharedPreferences prefs;

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  //static GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  SharedPreferences prefs;
  bool isLogin;
  int _currentIndex = 0;


  @override
  Future initState() {
    // TODO: implement initState
    super.initState();
    _bottomNavBarBloc = BottomAppBarBloc();
    isLogin = false;
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _bottomNavBarBloc.close();
    super.dispose();
  }

  BottomAppBarBloc _bottomNavBarBloc;
  bool showUserDetails = false;

  final List<Widget> _children = [
    HomeWidget(),
    CategoriesWidget(),
    FavoritosWidget(),
    OfertasWidget(),
  ];

  @override
  Widget build(BuildContext context) {
    getPreferences();
    return SafeArea(
      child: Scaffold(
        // key: _scaffoldKey,
        drawer: Container(
          width: MediaQuery.of(context).size.width * 0.65,
          child: Drawer(
            // Add a ListView to the drawer. This ensures the user can scroll
            // through the options in the drawer if there isn't enough vertical
            // space to fit everything.
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: <Widget>[
                Container(
                  color: primary,
                  height: 195,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 18.0),
                    child: UserAccountsDrawerHeader(
                      currentAccountPicture: isLogin
                          ? CircleAvatar(
                              radius: 30.0,
                              backgroundImage: NetworkImage(prefs
                                      .getString("user_avatar") ??
                                  'https://pwcenter.org/sites/default/files/default_images/default_profile.png'),
                              backgroundColor: Colors.transparent,
                            )
                          : CircleAvatar(
                              radius: 30.0,
                              backgroundImage: NetworkImage(
                                  'https://pwcenter.org/sites/default/files/default_images/default_profile.png'),
                              backgroundColor: Colors.transparent,
                            ),
                      accountName: isLogin
                          ? Text(prefs.getString("user_name"))
                          : GestureDetector(
                              onTap: () => goToCarrito(context),
                              child: Text('Iniciar Sesion'),
                            ),
                      accountEmail: Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: isLogin
                            ? Text(prefs.getString("user_email"))
                            : GestureDetector(
                                onTap: () => goToCarrito(context),
                                child: Text('Toca para iniciar sesion'),
                              ),
                      ),
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                ListTile(
                  title: Row(
                    children: <Widget>[
                      Icon(Icons.face),
                      Text('  Ver Perfil'),
                    ],
                  ),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: Row(
                    children: <Widget>[
                      Icon(Icons.add_shopping_cart),
                      Text('  Pedidos'),
                    ],
                  ),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                    goToPedidos(context);
                  },
                ),
                ListTile(
                  title: Row(
                    children: <Widget>[
                      Icon(Icons.history),
                      Text('  Historial'),
                    ],
                  ),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                    goToHistorial(context);
                  },
                ),
                isLogin
                    ? ListTile(
                        title: Row(
                          children: <Widget>[
                            Icon(Icons.close),
                            Text('  Cerrar sesión'),
                          ],
                        ),
                        onTap: () {
                          closeSession();
                          // Update the state of the app.
                          // ...
                        },
                      )
                    : Container(),
              ],
            ),
          ),
        ),
        backgroundColor: Home.background,
        body: Column(
          children: <Widget>[
            toolbar(context),
            SizedBox(height: 10,),
            StreamBuilder<NavBarItem>(
              stream: _bottomNavBarBloc.itemStream,
              initialData: _bottomNavBarBloc.defaultItem,
              builder: (BuildContext context, AsyncSnapshot<NavBarItem> snapshot) {
                switch (snapshot.data) {
                  case NavBarItem.Home:
                    return _children[0];
                  case NavBarItem.Categories:
                    return _children[1];
                  case NavBarItem.Favorites:
                    return _children[2];
                  case NavBarItem.Ofers:
                    return _children[3];
                }
                return Container();
              },
            ),
          ],
        ),
        bottomNavigationBar:
            MyBottomNav(index: 0, onTap: _bottomNavBarBloc.pickItem),
        floatingActionButton: FloatingActionButton(
          onPressed: () => goToCarrito(context),
          backgroundColor: Home.accent,
          child: Icon(Icons.shopping_cart),
        ),
      ),
    );
  }

  void onTabBottomNavItem(int value) {
    print("posicion numero " + value.toString());
    setState(() {
      _currentIndex = value;
    });
  }


  Widget toolbar(BuildContext context) => Container(
    height: 50,
    child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
            padding: const EdgeInsets.only(top: 10, left: 25),
            child: Builder(
              builder :(context)=> IconButton(icon: Icon(Menu.menu), onPressed: () => Scaffold.of(context).openDrawer()

                //_scaffoldKey.currentState.openDrawer()
              ),
            )),
        Padding(
          padding: const EdgeInsets.only(top: 10, right: 30),
          child: IconButton(icon: Icon(Search.magnifier), onPressed: () => goToSearch())
          ,
        )
      ],
    ),
  );

  goToCarrito(BuildContext context) async {
    prefs = await SharedPreferences.getInstance();
    bool isLogin = prefs.getBool('isLogin');

    if (isLogin == null) {
      prefs.setBool('isLogin', false);
      isLogin = false;
    }

    if (!isLogin) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginWidget()),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => CarritoWidget()),
      );
    }
  }

  Future getPreferences() async {
    prefs = await SharedPreferences.getInstance();
    bool esta_logeado = prefs.getBool('isLogin') ?? false;
    setState(() {
      isLogin = esta_logeado;
    });
  }

  void closeSession() async {
    await prefs.setString('token', "");
    await prefs.setString('user_name', "");
    await prefs.setString('user_email', "");
    await prefs.setString('user_avatar', "");
    await prefs.setBool('isLogin', false);
    setState(() {
      isLogin = false;
      Navigator.pop(context);
    });
  }

  /* isLogin() async{
    prefs = await SharedPreferences.getInstance();
    bool login=prefs.getBool("isLogin")?? false;
    if(login){
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => CarritoWidget()),
      );
    }else{
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginWidget()),
      );
    }
  }*/

  void goToPedidos(BuildContext context) async {
    prefs = await SharedPreferences.getInstance();
    bool isLogin = prefs.getBool('isLogin');

    if (isLogin == null) {
      prefs.setBool('isLogin', false);
      isLogin = false;
    }

    if (!isLogin) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginWidget()),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => OrderWidget()),
      );
    }
  }

  void goToHistorial(BuildContext context) async {
    prefs = await SharedPreferences.getInstance();
    bool isLogin = prefs.getBool('isLogin');

    if (isLogin == null) {
      prefs.setBool('isLogin', false);
      isLogin = false;
    }

    if (!isLogin) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginWidget()),
      );
    } else {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => HistorialWidget()),
      );
    }
  }

  goToSearch() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SearchWidget()),
    );
  }
}
