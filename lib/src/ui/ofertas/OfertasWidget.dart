
import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/ui/ofertas/ListOfertasWidget.dart';
import 'package:shared_preferences/shared_preferences.dart';



class OfertasWidget extends StatefulWidget {
  @override
  _OfertasWidgetState createState() => _OfertasWidgetState();
}

class _OfertasWidgetState extends State<OfertasWidget> {

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListOfertasWidget(),
    );
  }

}
