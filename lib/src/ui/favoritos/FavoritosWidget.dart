

import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/blocs/favoritos_bloc.dart';
import 'package:samer_client_flutter/src/database/repository_service_product.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'package:samer_client_flutter/src/ui/principal_widget/CardPrincpal.dart';

class FavoritosWidget extends StatefulWidget {
  @override
  _FavoritosWidgetState createState() => _FavoritosWidgetState();
}

class _FavoritosWidgetState extends State<FavoritosWidget> {
  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  static Color accent= Color.fromRGBO(255, 189, 25,1);
  Future<List<ProductoEntity>> future;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    favoritos_bloc.fetchAllFavoritos();

  }

  @override
  void dispose() {
    super.dispose();
  }




  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10.0,
          ),
          child: StreamBuilder(
            stream: favoritos_bloc.allFavoritos,
            builder: (context, AsyncSnapshot<ProductsResults> snapshot) {
              if (snapshot.hasData) {
                return buildList(snapshot);
              } else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }
              return Center(child: CircularProgressIndicator());
            },
          )),
    );
  }

  Widget buildList(AsyncSnapshot<ProductsResults> snapshot) {
    return GridView.builder(
        itemCount: snapshot.data.results.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 0.65),
        itemBuilder: (BuildContext context, int index) {
          return  buildCard(snapshot.data.results[index]);
          //onTap: () => openDetailPage(snapshot.data, index),
        });
  }

  buildCard(ProductoEntity productoEntity) {
    Future<int> cantidad=  RepositoryServiceTodo.ProductCount(productoEntity.pk);
    return FutureBuilder<int>(future: cantidad,builder:(context,snapshot){

      return CardPrincipal(productoEntity);

    });
  }
}
