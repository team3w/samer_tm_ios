

class CategoriesResults{
  List<CategoryEntity> _data;

  CategoriesResults.fromJson(Map<String, dynamic> parsedJson){
    List<CategoryEntity> temp = [];
    for (int i = 0; i < parsedJson['data'].length; i++) {
      CategoryEntity result = CategoryEntity(parsedJson['data'][i]);
      temp.add(result);
    }
    _data = temp;
  }
  List<CategoryEntity> get data => _data;
}


class CategoryEntity {
  int _pk;
  String _title;
  String _image;



  CategoryEntity(parsedJson) {
    _pk = parsedJson['pk'];
    _title = parsedJson['title'];
    _image = parsedJson['image'];
  }

  int get pk => _pk;

  String get title => _title;

  String get image => _image;


}