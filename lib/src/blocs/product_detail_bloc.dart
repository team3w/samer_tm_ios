
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';

import '../resources/Repository.dart';

class ProductsDetailBloc {
  final _repository = Repository();
  final _productsFetcher = PublishSubject<ProductoEntity>();

  Stream<ProductoEntity> get allProductsDetail => _productsFetcher.stream;

  fetchProductDetail(int pk) async {
    if(_isDisposed) {
      return;
    }
    ProductoEntity itemModel = await _repository.fetchProductDetail(pk);
    _productsFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;
  void dispose() {
    _productsFetcher.close();
    _isDisposed = true;
  }

}

final product_detail_bloc = ProductsDetailBloc();