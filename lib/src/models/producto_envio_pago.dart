import 'package:samer_client_flutter/src/database/database_creator.dart';



class EnvioPagoMontoEntity{

  List<ProductoEnvioPago> _list_product;

  EnvioPagoMontoEntity.fromJson(Map<String, dynamic> parsedJson){
    List<ProductoEnvioPago> temp = [];
    for (int i = 0; i < parsedJson['list_product'].length; i++) {
      ProductoEnvioPago result = ProductoEnvioPago(parsedJson['list_product'][i]);
      temp.add(result);
    }
    _list_product = temp;
  }

  List<ProductoEnvioPago> get list_product => _list_product;


}




class ProductoEnvioPago {
  int _id_product;
  int _quantity;


  ProductoEnvioPago.constructor(this._id_product, this._quantity);

  ProductoEnvioPago(parsedJson) {
    //print(parsedJson.toString());
    _id_product = parsedJson['id_product'];
    _quantity = parsedJson['quantity'];


  }

  Map<String, String> toJson() => {'id_product':  _id_product.toString(), 'quantity': _quantity.toString()};

  @override
  String toString() {
    return 'ProductoEnvioPago{_id_product: $_id_product, _quantity: $_quantity}';
  }

  int get quantity => _quantity;

  int get id_product => _id_product;


}
