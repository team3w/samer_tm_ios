
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';

import '../resources/Repository.dart';

class FavoritosBloc {
  final _repository = Repository();
  final _productsFetcher = PublishSubject<ProductsResults>();

  Stream<ProductsResults> get allFavoritos => _productsFetcher.stream;

  fetchAllFavoritos() async {
    if(_isDisposed) {
      return;
    }
    ProductsResults itemModel = await _repository.fetchFavoritos();
    _productsFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;
  void dispose() {
    _productsFetcher.close();
    _isDisposed = true;
  }

}

final favoritos_bloc = FavoritosBloc();