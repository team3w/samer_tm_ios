
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/order_entity.dart';
import '../resources/Repository.dart';

class OrdersBloc {
  final _repository = Repository();
  final _orderFetcher = PublishSubject<OrdersResults>();

  Stream<OrdersResults> get getOrdersUser => _orderFetcher.stream;

  fetchOrders() async {
    if(_isDisposed) {
      return;
    }
    OrdersResults itemModel = await _repository.fetchOrders();
    _orderFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;

  void dispose() {
    _orderFetcher.close();
    _isDisposed = true;
  }

}

final blocOrders = OrdersBloc();