import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../CONSTANTS.dart';


class VoucherWidget extends StatefulWidget {
  String code;
  VoucherWidget(this.code);

  @override
  _VoucherWidgetState createState() => _VoucherWidgetState(code);
}

class _VoucherWidgetState extends State<VoucherWidget> {
  String code;
  _VoucherWidgetState(this.code);
  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  static TextEditingController numeroOperacionController =
      TextEditingController();
  File _image;
  String _photo;
  static CONSTANTS constants=CONSTANTS();


  @override
  void initState() {
    numeroOperacionController.text="";
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    List<int> imageBytes = image.readAsBytesSync();
    print(imageBytes);
    String base64Image = base64Encode(imageBytes);

    setState(() {
      _image = image;
      _photo = base64Image;
    });
  }

  Widget numero_operacion = Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.symmetric(vertical: 10.0),
        child: Text(
          "Número de operación ",
          style: TextStyle(
            color: primary,
            fontSize: 15,
            fontWeight: FontWeight.w500,
          ),
        ),
      ),
      ClipRRect(
        borderRadius: BorderRadius.circular(15.0),
        child: Container(
          color: Colors.black12,
          child: Container(
              child: Padding(
            padding: const EdgeInsets.only(left: 18.0, right: 18.0, top: 10),
            child: TextField(
              controller: numeroOperacionController,
              keyboardType: TextInputType.number,
              inputFormatters: <TextInputFormatter>[
                WhitelistingTextInputFormatter.digitsOnly,
                LengthLimitingTextInputFormatter(11)
              ],
              style: TextStyle(fontSize: 18),
            ),
          )),
        ),
      )
    ],
  );

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Scaffold(bottomNavigationBar: RaisedButton(
            color: Colors.green,
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 20.0),
              child: Text(
                "SUBIR VOUCHER",
                style: TextStyle(color: Colors.white),
              ),
            ),
            onPressed: () => sendVoucher()),
            appBar: AppBar(
              title: Text('Voucher pedido N°' + code),
              centerTitle: false,
            ),
            body: Container(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: ListView(
                  children: <Widget>[
                    SizedBox(
                      height: 15,
                    ),
                    numero_operacion,
                    SizedBox(
                      height: 10,
                    ),
                    RaisedButton(
                        color: primary,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(vertical: 13.0),
                          child: Text(
                            "Subir imagen",
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                        onPressed: () => getImage()),
                    SizedBox(
                      height: 10,
                    ),
                    Center(
                      child: Container(
                        height: 380,
                        child: _image == null
                            ? Text('No has seleccionado Imagen')
                            : Image.file(_image),
                      ),
                    ),


                  ],
                ),
              ),
            )));
  }

  sendVoucher() {

    bool isValid= validation();
    if(isValid){
        postVoucher();
    }else{
      Fluttertoast.showToast(
          msg: 'No paso validacion', toastLength: Toast.LENGTH_SHORT);
    }
  }

  bool validation() {
    if(numeroOperacionController.text.length==0){
      Fluttertoast.showToast(
          msg: 'Número de operación necesario', toastLength: Toast.LENGTH_SHORT);
      return false;
    }
    if(_image==null){
      Fluttertoast.showToast(
          msg: 'Imagen necesaria', toastLength: Toast.LENGTH_SHORT);
      return false;
    }
    return true;

  }

  void postVoucher() async{
    Client client = Client();
    //final _apiKey = '802b2c4b88ea1183e50e6b285a27696e';
    String todayDay = DateTime.now().day.toString();
    String todayMonth = DateTime.now().month.toString();
    String todayYear = DateTime.now().year.toString();

    var _baseUrl=constants.URL;
    var api = "api/v1.0/shop/checkout/mobile/voucher/";
    SharedPreferences prefs;
    prefs = await SharedPreferences.getInstance();
    prefs = await SharedPreferences.getInstance();
    String token =  prefs.getString('token');



    var body = json.encode({
      "date_operation": "$todayYear-$todayMonth-$todayDay"+" 0" + ":" + "0",
      'operation_number': numeroOperacionController.text,
      "id_order": int.parse(code),
      'image': _photo
    });

    Map<String, String> headers={
      'Authorization': "bearer "+token,
      "Content-Type": "application/json"

    };

    final response =
        await client.post(_baseUrl+api.toString(),body: body,headers:headers );

    print(response.body);
    print(response.statusCode.toString());
    if (response.statusCode == 200) {
      Fluttertoast.showToast(msg: 'Voucher subido correctamente',toastLength: Toast.LENGTH_SHORT);
      Navigator.pushNamedAndRemoveUntil(
          context,
          '/initial',
              (Route<dynamic> route) => false);

    }else {
      // If that call was not successful, throw an error.
      print(body);
      print(response.body);
      throw Exception('Failed to load post');
    }
  }
}
