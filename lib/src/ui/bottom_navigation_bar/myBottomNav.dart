import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/icons/store_icons.dart';
import 'package:samer_client_flutter/src/icons/home_icons.dart';
import 'package:samer_client_flutter/src/icons/heart_icons.dart';
import 'package:samer_client_flutter/src/icons/diamond_icons.dart';

class MyBottomNav extends StatefulWidget {
  var index = 0;

  MyBottomNav({int index, this.onTap}) {
    this.index = index;
  }

  final ValueChanged<int> onTap;

  @override
  _MyBottomNavState createState() => _MyBottomNavState(index, onTap);
}

class _MyBottomNavState extends State<MyBottomNav> {
  Color primary = Color.fromRGBO(54, 4, 69, 0.7);

  int index = 0;
  List<NavigationItem> items = [
    NavigationItem(Icon(Home.home), Text("Principal")),
    NavigationItem(Icon(Store.store), Text("Categorias")),
    NavigationItem(Icon(Heart.heart), Text("Favoritos")),
    NavigationItem(Icon(Diamond.diamond), Text("Ofertas")),
  ];

  _MyBottomNavState(i, this.onTap) {
    index = i;
  }
  final ValueChanged<int> onTap;

  Widget _BuildItem(NavigationItem navigationItem, bool isSelected) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 19.0),
      child: AnimatedContainer(
        width: !isSelected ? 45 : 125,
        duration: Duration(milliseconds: 270),
        decoration: isSelected
            ? BoxDecoration(
                color: primary,
                borderRadius: BorderRadius.all(Radius.circular(40.0)))
            : null,
        child: Padding(
          padding: const EdgeInsets.all(1.0),
          child: ListView(
            scrollDirection: Axis.horizontal,
            children: <Widget>[
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: 14, bottom: 4),
                    child: IconTheme(
                      data: IconThemeData(
                          size: 22,
                          color: isSelected ? Colors.white : Colors.black),
                      child: navigationItem.icon,
                    ),
                  ),
                  isSelected
                      ? Padding(
                          padding: const EdgeInsets.only(left: 7.0, right: 5.0),
                          child: DefaultTextStyle.merge(
                              style: TextStyle(
                                  color:
                                      isSelected ? Colors.white : Colors.black),
                              child: navigationItem.title),
                        )
                      : Container()
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,
          boxShadow: [BoxShadow(color: Colors.black12, blurRadius: 4)]),
      width: MediaQuery.of(context).size.width,
      height: 80,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 30.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: items.map((item) {
            var itemIndex = items.indexOf(item);
            return GestureDetector(
                onTap: () {
                  onTap(itemIndex);
                  setState(() {
                    index = itemIndex;
                  });
                },
                child: _BuildItem(item, index == itemIndex));
          }).toList(),
        ),
      ),
    );
  }
}

class NavigationItem {
  final Icon icon;
  final Text title;

  NavigationItem(this.icon, this.title);
}
