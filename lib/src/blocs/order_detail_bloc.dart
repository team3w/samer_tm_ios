
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/DetalleResponseEntity.dart';
import '../resources/Repository.dart';

class OrdersDetailBloc {
  final _repository = Repository();
  final _orderFetcher = PublishSubject<DetalleResponseEntity>();

  Stream<DetalleResponseEntity> get getOrdersDetail => _orderFetcher.stream;

  fetchOrdersDetail(int pk) async {
    if(_isDisposed) {
      return;
    }
    DetalleResponseEntity itemModel = await _repository.fetchOrderDetail(pk);
    _orderFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;

  void dispose() {
    _orderFetcher.close();
    _isDisposed = true;
  }

}

final blocOrdersDetail = OrdersDetailBloc();