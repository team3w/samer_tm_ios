import 'dart:io';

import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

Database db;

class DatabaseCreator {
  static const productTable = 'product';
  static const pk = 'pk';
  static const title_products = 'title_products';
  static const id_brand = 'id_brand';
  static const base_price = 'base_price';
  static const image = 'image';
  static const id_unit = 'id_unit';
  static const quantity = 'quantity';



  static void databaseLog(String functionName, String sql,
      [List<Map<String, dynamic>> selectQueryResult, int insertAndUpdateQueryResult, List<dynamic> params]) {
    print(functionName);
    print(sql);
    if (params != null) {
      print(params);
    }
    if (selectQueryResult != null) {
      print(selectQueryResult);
    } else if (insertAndUpdateQueryResult != null) {
      print(insertAndUpdateQueryResult);
    }
  }

  Future<void> createTodoTable(Database db) async {
    final todoSql = '''CREATE TABLE $productTable
    (
      $pk INTEGER PRIMARY KEY,                       
      $title_products TEXT,   
      $id_brand  TEXT,  
      $base_price  FLOAT, 
      $image   TEXT, 
      $id_unit  TEXT,
      $quantity INTEGER
    )''';


    await db.execute(todoSql);
  }

  Future<String> getDatabasePath(String dbName) async {
    final databasePath = await getDatabasesPath();
    final path = join(databasePath, dbName);

    //make sure the folder exists
    if (await Directory(dirname(path)).exists()) {
      //await deleteDatabase(path);
    } else {
      await Directory(dirname(path)).create(recursive: true);
    }
    return path;
  }


  Future<void> initDatabase() async {
    final path = await getDatabasePath('products_db');
    db = await openDatabase(path, version: 1, onCreate: onCreate);
    print(db);
  }



  Future<void> onCreate(Database db, int version) async {
    await createTodoTable(db);
  }
}

