
import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/blocs/sub_categories_bloc.dart';
import 'package:samer_client_flutter/src/models/sub_category_entity.dart';

import 'LowCategoriesWidget.dart';

class SubCategoriesWidget extends StatefulWidget {

  int pk;
  SubCategoriesWidget(int pk){
    this.pk=pk;
  }

  @override
  _SubCategoriesWidgetState createState() => _SubCategoriesWidgetState(pk);
}

class _SubCategoriesWidgetState extends State<SubCategoriesWidget> {
  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  static Color accent= Color.fromRGBO(255, 189, 25,1);

  int pk;


  _SubCategoriesWidgetState(int pk){
    this.pk=pk;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sub_categories_bloc.fetchAllSubCategories(pk);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 10.0,
              ),
              child: StreamBuilder(
                stream: sub_categories_bloc.allSubCatergories,
                builder: (context, AsyncSnapshot<SubCategoriesResults> snapshot) {
                  if (snapshot.hasData) {
                    return  ListView(
                      children: builCardsOrders(snapshot.data),
                    );
                  } else if (snapshot.hasError) {
                    return Text(snapshot.error.toString());
                  }
                  return Center(child: CircularProgressIndicator());
                },
              )),
        ),
      ),
    );
  }

  builCardsOrders(SubCategoriesResults data) {
    List<Widget> lista = [];
    lista.add(SizedBox(height: 30,));
    for (int i = 0; i < data.data.length; i++) {
      SubCategoryEntity order= data.data[i];
      lista.add(Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0,vertical: 4),
        child: SizedBox(
          width: double.infinity,
          height: 80,
          child: GestureDetector(
            onTap: ()=>goToSubCategories(order.children),
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding:
                    const EdgeInsets.only(top: 18.0, left: 20, bottom: 5),
                    child: Text(
                      "N° "+order.pk.toString(),
                      style: TextStyle(fontWeight: FontWeight.w800),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0,right: 10),
                        child: Text(order.title,overflow: TextOverflow.ellipsis,),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ));
    }
    return lista;
  }

  goToSubCategories(List<LowCategoryEntity> children ) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => LowCategoriesWidget(children)),
    );
  }

}
