import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/blocs/order_bloc.dart';
import 'package:samer_client_flutter/src/models/order_entity.dart';
import 'package:samer_client_flutter/src/ui/order_detail/OrderDetailWidget.dart';

class OrderWidget extends StatefulWidget {
  @override
  _OrderWidgetState createState() => _OrderWidgetState();
}

class _OrderWidgetState extends State<OrderWidget> with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    _controller = AnimationController(vsync: this);
    super.initState();
    blocOrders.fetchOrders();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Mis pedidos'),
          centerTitle: false,
        ),        body: Container(
          child: StreamBuilder(
            stream: blocOrders.getOrdersUser,
            builder: (context, AsyncSnapshot<OrdersResults> snapshot) {
              if (snapshot.hasData) {
                return  ListView(
                  children: builCardsOrders(snapshot.data),
                );
              } else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }
              return Center(child: CircularProgressIndicator());
            },
          ),

        ));
  }

  builCardsOrders(OrdersResults data) {
    List<Widget> lista = [];
    lista.add(SizedBox(height: 10,));
    for (int i = 0; i < data.results.length; i++) {
      OrderEntity order= data.results[i];
      lista.add(GestureDetector(
        onTap: ()=>goToDetail(order.pk),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 12.0,vertical: 4),
          child: SizedBox(
            width: double.infinity,
            height: 80,
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding:
                    const EdgeInsets.only(top: 18.0, left: 20, bottom: 5),
                    child: Text(
                      "N° "+order.pk.toString(),
                      style: TextStyle(fontWeight: FontWeight.w800),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0,right: 10),
                        child: Text(order.id_region,overflow: TextOverflow.ellipsis,),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ));
    }
    return lista;
  }

  goToDetail(int pk) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => OrderDetailWidget(pk)),
    );

  }
}
