
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/order_entity.dart';
import '../resources/Repository.dart';

class HistorialBloc {
  final _repository = Repository();
  final _historialFetcher = PublishSubject<OrdersResults>();

  Stream<OrdersResults> get getHistorialUser => _historialFetcher.stream;

  fetchHistorial() async {
    if(_isDisposed) {
      return;
    }
    OrdersResults itemModel = await _repository.fetchHistorial();
    _historialFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;

  void dispose() {
    _historialFetcher.close();
    _isDisposed = true;
  }

}

final blocHistorial = HistorialBloc();