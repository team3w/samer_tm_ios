import 'dart:async';
import 'package:http/http.dart' show Client;
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'dart:convert';

import '../CONSTANTS.dart';

class ProductoDetailApiProvider {

  var api = "api/v1.0/shop/mobile/products/detail/";
  Client client = Client();
  static CONSTANTS constants=CONSTANTS();
  var _baseUrl=constants.URL;

  Future<ProductoEntity> fetchProducts(int pk) async {
    final response = await client.get(_baseUrl+api.toString()+pk.toString());
    print(response.body.toString());
    if (response.statusCode == 200) {
      // Success :)
      return ProductoEntity.fromJson(json.decode(utf8.decode(response.bodyBytes)));
    } else {
      // Not Success :(
      throw Exception('Failed to load post');
    }
  }
  
}