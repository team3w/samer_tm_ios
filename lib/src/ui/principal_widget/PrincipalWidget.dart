import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:samer_client_flutter/src/icons/search_icons.dart';
import 'package:samer_client_flutter/src/icons/menu_icons.dart';
import 'package:samer_client_flutter/src/ui/login/LoginWidget.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'ListHomeWidget.dart';

class HomeWidget extends StatefulWidget {
  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);

  @override
  _HomeWidgetState createState() => _HomeWidgetState();
}

class _HomeWidgetState extends State<HomeWidget> {
  SharedPreferences prefs;

  @override
  void initState() {
    super.initState();
  }

  Widget toolbar() => Container(
        height: 70,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(top: 5, left: 25),
                child: IconButton(icon: Icon(Menu.menu), onPressed: () => {}
                    //_scaffoldKey.currentState.openDrawer()
                    )),
            Padding(
              padding: const EdgeInsets.only(top: 5, right: 30),
              child: Icon(Search.magnifier),
            )
          ],
        ),
      );

  Widget title = Container(
      child: Row(
    children: <Widget>[
      Padding(
        padding: const EdgeInsets.only(left: 30, top: 1, bottom: 10),
        child: Text(
          "Principal",
          style: TextStyle(
            color: HomeWidget.primary,
            fontSize: 24,
            fontWeight: FontWeight.bold,
          ),
        ),
      )
    ],
  ));

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListHomeWidget(),
    );
  }

  Future<void> toLogin(BuildContext context) async {
    prefs = await SharedPreferences.getInstance();
    bool isLogin = prefs.getBool('isLogin');

    if (isLogin == null) {
      prefs.setBool('isLogin', false);
      isLogin = false;
    }

    if (!isLogin) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => LoginWidget()),
      );
    } else {
      Fluttertoast.showToast(
          msg: 'Ya estas logeado', toastLength: Toast.LENGTH_SHORT);
    }
  }
}
