
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/direction_entity.dart';
import '../resources/Repository.dart';

class DirectionUserBloc {
  final _repository = Repository();
  final _directionFetcher = PublishSubject<DirectionResults>();

  Stream<DirectionResults> get getDirectionUser => _directionFetcher.stream;

  fetchDirectionUser() async {
    if(_isDisposed) {
      return;
    }
    DirectionResults itemModel = await _repository.fetchDirectionUser();
    _directionFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;

  void dispose() {
    _directionFetcher.close();
    _isDisposed = true;
  }

}

final blocDirection = DirectionUserBloc();