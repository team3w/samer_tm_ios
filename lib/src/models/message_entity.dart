class MessageEntity {
  String _message;
  int _id_location;

  MessageEntity.fromJson(Map<String, dynamic> parsedJson) {
    print("message entity post direction : "+parsedJson['message']);
    _message = parsedJson['message'];
    _id_location = parsedJson['id_location'];

  }

  String get message => _message;

  int get id_location => _id_location;


}