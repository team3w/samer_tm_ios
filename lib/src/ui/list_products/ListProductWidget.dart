
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:samer_client_flutter/src/blocs/list_product_bloc.dart';
import 'package:samer_client_flutter/src/blocs/ofertas_bloc.dart';
import 'package:samer_client_flutter/src/database/repository_service_product.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'package:samer_client_flutter/src/models/list_response.dart';
import 'package:samer_client_flutter/src/ui/principal_widget/CardPrincpal.dart';

class ListProductWidget extends StatefulWidget {

  int pk;
  String title;
  ListProductWidget(int pk, String title){
    this.pk=pk;
    this.title=title;
  }

  @override
  _ListProductWidgetState createState() => _ListProductWidgetState(pk,title);
}

class _ListProductWidgetState extends State<ListProductWidget> {

  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  static Color accent= Color.fromRGBO(255, 189, 25,1);
  Future<List<ProductoEntity>> future;
  int pk;
  String title;


  _ListProductWidgetState(int pk, String title){
    this.pk=pk;
    this.title=title;

  }


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    list_product_bloc.fetchListProducts(pk);

  }

  @override
  void dispose() {
    super.dispose();
  }




  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(appBar: AppBar(title: Text(capitalize(title.toLowerCase())),),
        body: Container(
          child: Padding(
              padding: const EdgeInsets.only(
                left: 10.0,right: 10,top: 30
              ),
              child: StreamBuilder(
                stream: list_product_bloc.allListProduct,
                builder: (context, AsyncSnapshot<ListResponse> snapshot) {
                  if (snapshot.hasData) {
                    return buildList(snapshot);
                  } else if (snapshot.hasError) {
                    return Text(snapshot.error.toString());
                  }
                  return Center(child: CircularProgressIndicator());
                },
              )),
        ),
      ),
    );
  }

  Widget buildList(AsyncSnapshot<ListResponse> snapshot) {
    return GridView.builder(
        itemCount: snapshot.data.products.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2, childAspectRatio: 0.65),
        itemBuilder: (BuildContext context, int index) {
          return  buildCard(snapshot.data.products[index]);
          //onTap: () => openDetailPage(snapshot.data, index),
        });
  }

  buildCard(ProductoEntity productoEntity) {
    Future<int> cantidad=  RepositoryServiceTodo.ProductCount(productoEntity.pk);
    return FutureBuilder<int>(future: cantidad,builder:(context,snapshot){

      return CardPrincipal(productoEntity);

    });
  }

  String capitalize(String s) => s[0].toUpperCase() + s.substring(1);
}
