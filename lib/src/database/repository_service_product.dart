import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'package:samer_client_flutter/src/database/database_creator.dart';

class RepositoryServiceTodo {

  static Future<List<ProductoEntity>> getAllTodos() async {
    final sql = '''SELECT * FROM ${DatabaseCreator.productTable}''';
    final data = await db.rawQuery(sql);
    List<ProductoEntity> todos = List();

    for (final node in data) {
      final todo = ProductoEntity.fromJson(node);
      todos.add(todo);
    }
    return todos;
  }

  static Future<ProductoEntity> getTodo(int id) async {
    //final sql = '''SELECT * FROM ${DatabaseCreator.todoTable}
    //WHERE ${DatabaseCreator.id} = $id''';
    //final data = await db.rawQuery(sql);

    final sql = '''SELECT * FROM ${DatabaseCreator.productTable}
    WHERE ${DatabaseCreator.pk} = ?''';

    List<dynamic> params = [id];
    final data = await db.rawQuery(sql, params);

    final todo = ProductoEntity.fromJson(data.first);
    return todo;
  }

  static Future<void> deleteAllTodos() async {
    final sql = '''DELETE FROM ${DatabaseCreator.productTable}''';
    final data = await db.rawQuery(sql);

  }

  static Future<void> deleteProduct(int pk) async {
    final sql = '''DELETE FROM ${DatabaseCreator.productTable}  WHERE ${DatabaseCreator.pk} = ?''';
    List<dynamic> params = [
      pk
    ];
    final data = await db.rawDelete(sql,params);
    DatabaseCreator.databaseLog('delete todo', sql, null, data, params);

  }

  static Future<void> addTodo(ProductoEntity todo) async {
    /*final sql = '''INSERT INTO ${DatabaseCreator.todoTable}
    (
      ${DatabaseCreator.id},
      ${DatabaseCreator.name},
      ${DatabaseCreator.info},
    )
    VALUES
    (
      ${todo.id},
      "${todo.name}",
      "${todo.info}",

    )''';*/

    final sql = '''INSERT INTO ${DatabaseCreator.productTable}
    (
      ${DatabaseCreator.pk},
      ${DatabaseCreator.title_products},
      ${DatabaseCreator.id_brand},
      ${DatabaseCreator.base_price},
      ${DatabaseCreator.image},
      ${DatabaseCreator.id_unit},
      ${DatabaseCreator.quantity}
    )
    VALUES (?,?,?,?,?,?,?)''';
    List<dynamic> params = [
      todo.pk,
      todo.title_products,
      todo.id_brand,
      todo.base_price,
      todo.image,
      todo.id_unit,
      1
    ];
    final result = await db.rawInsert(sql, params);
    DatabaseCreator.databaseLog('Add todo', sql, null, result, params);
  }



  static Future<int> updateTodo(ProductoEntity todo) async {
    /*final sql = '''UPDATE ${DatabaseCreator.todoTable}
    SET ${DatabaseCreator.name} = "${todo.name}"
    WHERE ${DatabaseCreator.id} = ${todo.id}
    ''';*/

    final sql1 = '''SELECT ${DatabaseCreator.quantity} FROM ${DatabaseCreator.productTable}   WHERE ${DatabaseCreator.pk} = ?''';

    List<dynamic> params1 = [todo.pk];
    final data = await db.rawQuery(sql1, params1);

    int count = data[0].values.elementAt(0);
    int idForNewItem = count++;


    final sql = '''UPDATE ${DatabaseCreator.productTable}
    SET ${DatabaseCreator.quantity} = ?
    WHERE ${DatabaseCreator.pk} = ?
    ''';

    List<dynamic> params = [idForNewItem+1, todo.pk];
    final result = await db.rawUpdate(sql, params);

    DatabaseCreator.databaseLog('Update todo', sql, null, result, params);
    return idForNewItem+1;
  }


  static Future<int> downgradeTodo(ProductoEntity todo) async {
    /*final sql = '''UPDATE ${DatabaseCreator.todoTable}
    SET ${DatabaseCreator.name} = "${todo.name}"
    WHERE ${DatabaseCreator.id} = ${todo.id}
    ''';*/

    final sql1 = '''SELECT ${DatabaseCreator.quantity} FROM ${DatabaseCreator.productTable}   WHERE ${DatabaseCreator.pk} = ?''';

    List<dynamic> params1 = [todo.pk];
    final data = await db.rawQuery(sql1, params1);

    int count = data[0].values.elementAt(0);
    int idForNewItem = count++;


    final sql = '''UPDATE ${DatabaseCreator.productTable}
    SET ${DatabaseCreator.quantity} = ?
    WHERE ${DatabaseCreator.pk} = ?
    ''';

    List<dynamic> params = [idForNewItem-1, todo.pk];
    final result = await db.rawUpdate(sql, params);

    DatabaseCreator.databaseLog('downgrade todo', sql, null, result, params);
    return idForNewItem-1;
  }




  static Future<int> todosCount() async {
    final data = await db
        .rawQuery('''SELECT COUNT(*) FROM ${DatabaseCreator.productTable}''');

    int count = data[0].values.elementAt(0);
    int idForNewItem = count++;
    return idForNewItem;
  }



  static Future<int> ProductCount(int id) async {
    final sql = '''SELECT ${DatabaseCreator.quantity} FROM ${DatabaseCreator.productTable}   WHERE ${DatabaseCreator.pk} = ?''';

    List<dynamic> params = [id];
    final data = await db.rawQuery(sql, params);

    try {
      int count =  data!=null ?data[0].values.elementAt(0):0;
      int idForNewItem = count++;
      return idForNewItem;

    } catch (error) {

      return 0;// executed for errors of all types other than Exception
    }



  }
}
