
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/monto_entity.dart';
import 'package:samer_client_flutter/src/models/producto_envio_pago.dart';
import '../resources/Repository.dart';

class PaymentsBloc {
  final _repository = Repository();
  final _directionFetcher = PublishSubject<MontoEntity>();

  Stream<MontoEntity> get getPaymentsUser => _directionFetcher.stream;

  fetchPaymentsUser(List<ProductoEnvioPago> list_product,int id_place) async {
    if(_isDisposed) {
      return;
    }
    MontoEntity itemModel = await _repository.fetchPayments(list_product, id_place);
    _directionFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;

  void dispose() {
    _directionFetcher.close();
    _isDisposed = true;
  }

}

final blocPayments = PaymentsBloc();