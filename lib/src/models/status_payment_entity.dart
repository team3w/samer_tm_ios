
class StatusPaymentEntity{

  int _status_payment;


  StatusPaymentEntity.fromJson(Map<String, dynamic> parsedJson) {
    _status_payment= parsedJson['status_payment'];
  }

  int get status_payment => _status_payment;

  @override
  String toString() {
    return 'StatusPaymentEntity{_status_payment: $_status_payment}';
  }


}