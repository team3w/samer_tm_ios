
import 'package:flutter/material.dart';


class ListSearchWidget extends StatefulWidget {
  String search;

  ListSearchWidget(String search){
    this.search=search;
  }

  @override
  _ListSearchWidgetState createState() => _ListSearchWidgetState(search);
}

class _ListSearchWidgetState extends State<ListSearchWidget> {
  String search;

  _ListSearchWidgetState(String search){
    this.search=search;

  }

  @override
  Widget build(BuildContext context) {
    return Container(child: Center(child: Text(search),),);
  }
}
