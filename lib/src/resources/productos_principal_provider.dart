import 'dart:async';
import 'package:http/http.dart' show Client;
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'dart:convert';

import '../CONSTANTS.dart';

class ProductoPrincipalApiProvider {
  Client client = Client();
  //final _apiKey = '802b2c4b88ea1183e50e6b285a27696e';

  static CONSTANTS constants=CONSTANTS();

  var _baseUrl=constants.URL;
  var api = "api/v1.0/shop/mobile/products/home";

  Future<ProductsResults> fetchProducts() async {
  print("entered");
  final response = await client
      .get(_baseUrl+api.toString());
  print(response.body.toString());
  if (response.statusCode == 200) {
  // If the call to the server was successful, parse the JSON
  return ProductsResults.fromJson(json.decode(utf8.decode(response.bodyBytes)));
  } else {
  // If that call was not successful, throw an error.
  throw Exception('Failed to load post');
  }
  }


}