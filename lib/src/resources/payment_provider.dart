import 'dart:async';
import 'package:http/http.dart' show Client;
import 'package:samer_client_flutter/src/models/monto_entity.dart';
import 'package:samer_client_flutter/src/models/order_entity.dart';
import 'package:samer_client_flutter/src/models/producto_envio_pago.dart';

import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

import '../CONSTANTS.dart';

class PaymentsProvider {
  Client client = Client();
  //final _apiKey = '802b2c4b88ea1183e50e6b285a27696e';

  static CONSTANTS constants=CONSTANTS();

  var _baseUrl=constants.URL;
  var api = "api/v1.0/shop/mobile/products/discount/";
  SharedPreferences prefs;



  Future<MontoEntity> fetchPayments(List<ProductoEnvioPago> list_product,int id_place) async {
    prefs = await SharedPreferences.getInstance();
    String token =  prefs.getString('token');



    var body = json.encode({
    "list_product": list_product,
    'id_places': id_place
    });

    Map<String, String> headers={
      'Authorization': "bearer "+token,
      "Content-Type": "application/json"

    };

    final response =
    await client.post(_baseUrl+api.toString(),body: body,headers:headers );

    print(response.body);
    print(response.statusCode.toString());
    if (response.statusCode == 200) {
      print(MontoEntity.fromJson(json.decode(response.body)).toString());
      MontoEntity montoEntity= MontoEntity.fromJson(json.decode(response.body));
      print(montoEntity.toString());
      return MontoEntity.fromJson(json.decode(response.body));

    }else {
      // If that call was not successful, throw an error.
      print(body);
      print(response.body);
      throw Exception('Failed to load post');
    }
  }


}