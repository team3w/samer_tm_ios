import 'dart:async';
import 'package:http/http.dart' show Client;
import 'package:samer_client_flutter/src/models/order_entity.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'dart:convert';

import '../CONSTANTS.dart';

class OrderProvider {
  Client client = Client();
  //final _apiKey = '802b2c4b88ea1183e50e6b285a27696e';

  static CONSTANTS constants=CONSTANTS();
  SharedPreferences prefs;

  var _baseUrl=constants.URL;
  var api = "api/v1.0/shop/mobile/history/order/ios/process/";

  Future<OrdersResults> fetchOrders() async {
    print("entered order provider");
    prefs = await SharedPreferences.getInstance();
    String token =  prefs.getString('token');

    Map<String, String> headers={
      'Authorization': "bearer "+token,
      "Content-Type": "application/json"

    };



    final response = await client
        .get(_baseUrl+api.toString(),headers: headers);
    print(response.body.toString());
    if (response.statusCode == 200) {
      // If the call to the server was successful, parse the JSON
      return OrdersResults.fromJson(json.decode(utf8.decode(response.bodyBytes)));
    } else {
      // If that call was not successful, throw an error.
      throw Exception('Failed to load post');
    }
  }


}