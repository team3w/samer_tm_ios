class CardEntity{

  String _card_number;
  String _cvv;
  int _expiration_month;
  int _expiration_year;
  String _email;

  String get card_number => _card_number;


  CardEntity(this._card_number, this._cvv, this._expiration_month,
      this._expiration_year, this._email);

  set card_number(String value) {
    _card_number = value;
  }

  String get cvv => _cvv;

  String get email => _email;

  set email(String value) {
    _email = value;
  }

  int get expiration_year => _expiration_year;

  set expiration_year(int value) {
    _expiration_year = value;
  }

  int get expiration_month => _expiration_month;

  set expiration_month(int value) {
    _expiration_month = value;
  }

  set cvv(String value) {
    _cvv = value;
  }


}