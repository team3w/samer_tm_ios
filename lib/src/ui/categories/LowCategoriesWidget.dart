
import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/models/sub_category_entity.dart';
import 'package:samer_client_flutter/src/ui/list_products/ListProductWidget.dart';

class LowCategoriesWidget extends StatefulWidget {

  List<LowCategoryEntity> children;

  LowCategoriesWidget(List<LowCategoryEntity> children){
    this.children=children;
  }

  @override
  _LowCategoriesWidgetState createState() => _LowCategoriesWidgetState(children);
}

class _LowCategoriesWidgetState extends State<LowCategoriesWidget> {
  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  static Color accent= Color.fromRGBO(255, 189, 25,1);

  int pk;
  List<LowCategoryEntity> children;


  _LowCategoriesWidgetState(List<LowCategoryEntity> children){
    this.children=children;
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
          child: Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 10.0,
              ),
            child: ListView(children: builCardsOrders(children),),

          ),
        ),
      ),
    );
  }

  builCardsOrders(List<LowCategoryEntity> children ) {
    List<Widget> lista = [];
    lista.add(SizedBox(height: 30,));
    for (int i = 0; i < children.length; i++) {
      LowCategoryEntity order= children[i];
      lista.add(Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0,vertical: 4),
        child: SizedBox(
          width: double.infinity,
          height: 80,
          child: GestureDetector(
            onTap: ()=>goToListProducts(order.pk,order.title),
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding:
                    const EdgeInsets.only(top: 18.0, left: 20, bottom: 5),
                    child: Text(
                      "N° "+order.pk.toString(),
                      style: TextStyle(fontWeight: FontWeight.w800),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0,right: 10),
                        child: Text(order.title,overflow: TextOverflow.ellipsis,),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ));
    }
    return lista;
  }

  goToListProducts(int pk, String title) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => ListProductWidget(pk,title)),
    );
  }

}
