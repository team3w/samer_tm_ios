
import 'dart:async';



enum NavBarItem {Home,Categories,Favorites,Ofers}

class BottomAppBarBloc {

  Stream<NavBarItem> get itemStream => _navBarController.stream;


  final StreamController<NavBarItem>  _navBarController = StreamController<NavBarItem>.broadcast();
  NavBarItem defaultItem=NavBarItem.Home;

  void pickItem(int i) {
    switch (i) {
      case 0:
        _navBarController.sink.add(NavBarItem.Home);
        break;
      case 1:
        _navBarController.sink.add(NavBarItem.Categories);
        break;
      case 2:
        _navBarController.sink.add(NavBarItem.Favorites);
        break;
      case 3:
        _navBarController.sink.add(NavBarItem.Ofers);
        break;
    }
  }
  close() {
    _navBarController?.close();
  }

}

