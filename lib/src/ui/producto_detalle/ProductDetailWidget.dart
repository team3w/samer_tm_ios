import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/blocs/product_detail_bloc.dart';
import 'package:samer_client_flutter/src/database/repository_service_product.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';

class ProductDetailWidget extends StatefulWidget {
  var pk;

  ProductDetailWidget(int pk) {
    this.pk = pk;
  }

  @override
  _ProductDetailWidgetState createState() => _ProductDetailWidgetState(pk);
}

class _ProductDetailWidgetState extends State<ProductDetailWidget> {
  var pk;
  int cantidad=0;

  _ProductDetailWidgetState(pk) {
    this.pk = pk;
    getQuantity();
  }


  void getQuantity() async{
    int cantidad=  await RepositoryServiceTodo.ProductCount(pk);

    setState(() {
      this.cantidad=cantidad;
    });
  }



  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    product_detail_bloc.fetchProductDetail(pk);
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Detalle de producto"),
          centerTitle: true,
        ),
        body: Container(
            child: StreamBuilder(
          stream: product_detail_bloc.allProductsDetail,
          builder: (context, AsyncSnapshot<ProductoEntity> snapshot) {
            if (snapshot.hasData) {
              return buildWidget(snapshot);
            } else if (snapshot.hasError) {
              return Text(snapshot.error.toString());
            }
            return Center(child: CircularProgressIndicator());
          },
        )),
      ),
    );
  }

  Widget buildWidget(AsyncSnapshot<ProductoEntity> snapshot) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          SizedBox(
            width: MediaQuery.of(context).size.width,
            height: 280,
            child: Image.network(snapshot.data.image),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 50.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(snapshot.data.id_brand, style: TextStyle(fontSize: 14)),
                SizedBox(
                  height: 5,
                ),
                Text(
                  snapshot.data.title_products,
                  style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10,
                ),
                Text(
                  "S/. " + snapshot.data.base_price.toString() + " c/u",
                  style: TextStyle(
                      fontSize: 19,
                      fontWeight: FontWeight.bold,
                      color: Colors.purple),
                ),
              ],
            ),
          ),
          SizedBox(height: 40,),
          Expanded(
            child: Align(alignment: Alignment.bottomCenter,
              child: SizedBox(
                width: MediaQuery.of(context).size.width,
                //height: 120,
                child: Container(
                  color: Colors.black12,
                  child: Column(crossAxisAlignment: CrossAxisAlignment.start,mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(top:25.0,left: 30),
                        child: Text("AGREGA AL CARRITO"),
                      ),
                      Row(children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left:30.0,top:10),
                          child: Text("Total a pagar por este producto:   "),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top:10.0),
                          child: Text("S/. "+(cantidad*snapshot.data.base_price).toStringAsFixed(3),style: TextStyle(color: Colors.purple,fontSize: 18),),
                        ),
                      ],),
                      Row(mainAxisAlignment: MainAxisAlignment.center,crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal:8.0,vertical:10 ),
                            child: GestureDetector(onTap: ()=>quitarProducto(snapshot.data),
                                //agregarBD(productoEntity),
                                child: IconTheme(
                                  data: IconThemeData(size: 35, color: Colors.redAccent),
                                  child: Icon(Icons.remove_circle),
                                )),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal:8.0,vertical: 16),
                            child: Text(
                                cantidad.toString(),
                                style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16)),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal:8.0,vertical: 10),
                            child: GestureDetector(onTap: ()=>agregarProducto(snapshot.data),
                                //agregarBD(productoEntity),
                                child: IconTheme(
                                  data: IconThemeData(size: 35, color: Colors.amber),
                                  child: Icon(Icons.add_circle),
                                )),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  quitarProducto(ProductoEntity product)  async{
    int quantity;
    if(cantidad>0){
      quantity= await RepositoryServiceTodo.downgradeTodo(product) ;
      print("cantidad disminuida a "+quantity.toString());

      setState(() {
        cantidad=cantidad-1;
      });
      if(cantidad==0){
        print("ahora debemos quitarlo");
        await RepositoryServiceTodo.deleteProduct(product.pk);
      }
    }

  }

  agregarProducto(ProductoEntity product) async{

    int cantidad2=  await RepositoryServiceTodo.ProductCount(product.pk);
    if(cantidad2>0){
      int quantity;
      quantity= await RepositoryServiceTodo.updateTodo(product) ;
      print("cantidad aumentada a "+quantity.toString());

      setState(() {
        cantidad=cantidad+1;
      });

    }else{
      await RepositoryServiceTodo.addTodo(product) ;
      print("agregado nuevo producto");
      setState(() {
        cantidad=1;
      });
    }


  }

}
