import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:samer_client_flutter/src/models/user_entity.dart';
import 'package:samer_client_flutter/src/ui/registrer/RegistrerWidget.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../CONSTANTS.dart';

class LoginWidget extends StatefulWidget {
  @override
  _LoginWidgetState createState() => _LoginWidgetState();
}

class _LoginWidgetState extends State<LoginWidget> {
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  static CONSTANTS constants=CONSTANTS();
  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  SharedPreferences prefs;
  String state="INGRESAR";



  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
            child: Container(
          color: Color.fromRGBO(133, 96, 170, 1),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 48.0),
            child: ListView(
              children: <Widget>[
                SizedBox(
                  height: 80,
                ),
                Image.asset(
                  'assets/icon_logo_white.png',
                  height: 100,
                ),
                SizedBox(
                  height: 60,
                ),
                TextField(
                  style: TextStyle(color: Colors.white, fontSize: 20),
                  controller: emailController,
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    labelText: "Correo",
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 20,
                ),
                TextField(
                  style: TextStyle(color: Colors.white, fontSize: 20),
                  controller: passwordController,
                  obscureText: true,
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                    enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.white),
                    ),
                    labelText: "Contraseña",
                    labelStyle: TextStyle(color: Colors.white),
                  ),
                ),
                SizedBox(
                  height: 50,
                ),
                Container(width: 80,
                  child: RaisedButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 10),
                      child: Text(state),
                    ),
                    color: primary,
                    textColor: Colors.white,
                    onPressed: ()=> login(context),
                  ),

                ),
                SizedBox(
                  height: 20,
                ),
                Container(width: 80,
                  child: RaisedButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 20.0, horizontal: 10),
                      child: Text("REGISTRARSE"),
                    ),
                    color: Color.fromRGBO(237, 174, 66, 0.9),
                    textColor: Colors.white,
                    onPressed: ()=> registrer(context),
                  ),
                )
              ],
            ),
          ),
        )),
      ),
    );
  }


  Future<void> login(BuildContext context) async {

    setState(() {
      this.state="INGRESANDO...";
    });

      prefs = await SharedPreferences.getInstance();

      Client client = Client();

      var _api = "api/v1.0/users/auth/client/login/";


      var _baseUrl=constants.URL;

      Map LogienEnvio={
        'username': emailController.text,
        'password': passwordController.text
      };

      final response =
      await client.post(_baseUrl+_api.toString(),body: LogienEnvio);

      if (response.statusCode == 200) {
        print(UserEntity.fromJson(json.decode(response.body)).toString());
        UserEntity userEntity= UserEntity.fromJson(json.decode(response.body));
        await prefs.setString('token', userEntity.token);
        await prefs.setString('user_name', userEntity.first_name);
        await prefs.setString('user_email', userEntity.email);
        await prefs.setString('user_avatar', userEntity.avatar);
        await prefs.setInt('id_group', userEntity.id_group);

        await prefs.setBool('isLogin', true);

        print("tu id group es "+userEntity.id_group.toString());
        Navigator.pop(context);

        //return UserEntity.fromJson(json.decode(response.body));

      }else if(response.statusCode == 400) {
        Fluttertoast.showToast(msg: 'Email o contraseña incorrecta',toastLength: Toast.LENGTH_SHORT);
      }
      else {
        Fluttertoast.showToast(msg: 'Error al conectarse con el servidor',toastLength: Toast.LENGTH_SHORT);
        print("${response.statusCode}");
        print("${response.body}");
        throw Exception('Failed to load trailers');
      }

    setState(() {
      this.state="INGRESAR";
    });

  }

  registrer(BuildContext context) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => RegisterWidget()),
    );
  }




}


