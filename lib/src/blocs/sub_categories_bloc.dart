
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/categoryEntity.dart';
import 'package:samer_client_flutter/src/models/sub_category_entity.dart';

import '../resources/Repository.dart';

class SubCategoriesBloc {
  final _repository = Repository();
  final _productsFetcher = PublishSubject<SubCategoriesResults>();

  Stream<SubCategoriesResults> get allSubCatergories => _productsFetcher.stream;

  fetchAllSubCategories(int pk) async {
    if(_isDisposed) {
      return;
    }
    SubCategoriesResults itemModel = await _repository.fetchSubCategories(pk);
    _productsFetcher.sink.add(itemModel);
  }

  bool _isDisposed = false;
  void dispose() {
    _productsFetcher.close();
    _isDisposed = true;
  }

}

final sub_categories_bloc = SubCategoriesBloc();