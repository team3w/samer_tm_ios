import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/app.dart';
import 'package:samer_client_flutter/src/database/database_creator.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await DatabaseCreator().initDatabase();
  runApp(App());

}
