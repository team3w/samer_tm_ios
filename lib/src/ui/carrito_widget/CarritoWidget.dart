import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:samer_client_flutter/src/database/repository_service_product.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'package:samer_client_flutter/src/ui/carrito_widget/CarritoCard.dart';
import 'package:samer_client_flutter/src/ui/direccion/DireccionWidget.dart';

class CarritoWidget extends StatefulWidget {
  @override
  _CarritoWidgetState createState() => _CarritoWidgetState();
}

class _CarritoWidgetState extends State<CarritoWidget> {
  Future<List<ProductoEntity>> future;
  int id;
  static Color accent = Color.fromRGBO(255, 189, 25, 1);
  String state = "Elegir lugar de envío";
  bool thereIsProducts = false;
  List<ProductoEntity> listProducts = List();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    future = RepositoryServiceTodo.getAllTodos();
    load();
    getCountProducts();
  }

  load() async {
    listProducts = await RepositoryServiceTodo.getAllTodos();
  }

  void readData() async {
    final todo = await RepositoryServiceTodo.getTodo(id);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: Container(
        height: 80,
        child: GestureDetector(
          onTap: () => sendDirection(context),
          child: Container(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              Container(
                height: 80,
                color: thereIsProducts ? Colors.green : Colors.grey,
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    state,
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.white, fontSize: 17),
                  ),
                ),
              )
            ],
          )),
        ),
      ),
      appBar: AppBar(
        title: Text("Carrito"),
      ),
      body: Padding(
        padding: const EdgeInsets.only(top: 18.0),
        child: Column(
          children: <Widget>[
            Expanded(
              child: FutureBuilder<List<ProductoEntity>>(
                  future: future,
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      return Column(
                          children: snapshot.data
                              .map((product) => buildItem(product))
                              .toList());
                    } else {
                      return SizedBox();
                    }
                    /*return ListView(
                          children: listProducts
                              .map((product) => buildItem(product))
                              .toList());*/
                  }),
            ),
          ],
        ),
      ),
    );
  }

  /*deleteItem(int pk,String name) async{
    //Fluttertoast.showToast(msg: 'Se eliminarà '+name,toastLength: Toast.LENGTH_SHORT);
    await RepositoryServiceTodo.deleteProduct(pk);
    listProducts = await RepositoryServiceTodo.getAllTodos();

    if (listProducts.length == 0) {
      setState(() {
        this.state = "No tienes productos en el carrito";
        this.thereIsProducts = false;
      });

    } else {
      setState(() {
        this.state = "Elegir lugar de envío";
        this.thereIsProducts = true;
      });
    }


  }*/

  sendDirection(BuildContext context) async {
    await getCountProducts();
    if (this.thereIsProducts) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => DireccionWidget()),
      );
    } else {
      Fluttertoast.showToast(
          msg: 'No tienes productos en el carrito',
          toastLength: Toast.LENGTH_SHORT);
    }
  }


  sendNotification(){
    Fluttertoast.showToast(
        msg: 'lo borro',
        toastLength: Toast.LENGTH_LONG);
  }

  getCountProducts() async {
    List<ProductoEntity> products = await RepositoryServiceTodo.getAllTodos();

    if (products.length == 0) {
      setState(() {
        this.state = "No tienes productos en el carrito";
        this.thereIsProducts = false;
      });
    } else {
      setState(() {
        this.state = "Elegir lugar de envío";
        this.thereIsProducts = true;
      });
    }
  }

  Widget buildItem(ProductoEntity product) {
    return Container(child: CardCarrito(product));
  }
}
