
import 'package:flutter/material.dart';
import 'package:samer_client_flutter/src/blocs/categories_bloc.dart';
import 'package:samer_client_flutter/src/models/categoryEntity.dart';

import 'SubCategoriesWidget.dart';

class CategoriesWidget extends StatefulWidget {
  @override
  _CategoriesWidgetState createState() => _CategoriesWidgetState();
}

class _CategoriesWidgetState extends State<CategoriesWidget> {
  static Color primary = Color.fromRGBO(54, 4, 69, 0.7);
  static Color accent= Color.fromRGBO(255, 189, 25,1);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    categories_bloc.fetchAllCategories();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: 10.0,
          ),
          child: StreamBuilder(
            stream: categories_bloc.allCatergories,
            builder: (context, AsyncSnapshot<CategoriesResults> snapshot) {
              if (snapshot.hasData) {
                return  ListView(
                  children: builCardsOrders(snapshot.data),
                );
              } else if (snapshot.hasError) {
                return Text(snapshot.error.toString());
              }
              return Center(child: CircularProgressIndicator());
            },
          )),
    );
  }

  builCardsOrders(CategoriesResults data) {
    List<Widget> lista = [];
    lista.add(SizedBox(height: 10,));
    for (int i = 0; i < data.data.length; i++) {
      CategoryEntity order= data.data[i];
      lista.add(Padding(
        padding: const EdgeInsets.symmetric(horizontal: 12.0,vertical: 4),
        child: SizedBox(
          width: double.infinity,
          height: 80,
          child: GestureDetector(
            onTap: ()=>goToSubCategories(order.pk),
            child: Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding:
                    const EdgeInsets.only(top: 18.0, left: 20, bottom: 5),
                    child: Text(
                      "N° "+order.pk.toString(),
                      style: TextStyle(fontWeight: FontWeight.w800),
                    ),
                  ),
                  Flexible(
                    child: Container(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0,right: 10),
                        child: Text(order.title,overflow: TextOverflow.ellipsis,),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ));
    }
    return lista;
  }

  goToSubCategories(int pk) {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SubCategoriesWidget(pk)),
    );
  }

}
