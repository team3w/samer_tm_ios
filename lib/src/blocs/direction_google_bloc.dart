
import 'package:rxdart/rxdart.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'package:samer_client_flutter/src/models/direction_google_model.dart';

import '../resources/Repository.dart';

class DirectionGoogleBloc {
  final _repository = Repository();
  final _directionFetcher = PublishSubject<DirectionResponseModel>();

  Stream<DirectionResponseModel> get getDirection => _directionFetcher.stream;

  fetchDirection(String latlng) async {
    if(_isDisposed) {
      return;
    }
    DirectionResponseModel itemModel = await _repository.fetchDirection(latlng);
    _directionFetcher.sink.add(itemModel);
  }


  bool _isDisposed = false;
  void dispose() {
    _directionFetcher.close();
    _isDisposed = true;
  }

}

final bloc = DirectionGoogleBloc();