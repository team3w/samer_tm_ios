import 'dart:async';
import 'package:samer_client_flutter/src/models/DetalleResponseEntity.dart';
import 'package:samer_client_flutter/src/models/ProductoEntity.dart';
import 'package:samer_client_flutter/src/models/categoryEntity.dart';
import 'package:samer_client_flutter/src/models/direction_entity.dart';
import 'package:samer_client_flutter/src/models/direction_google_model.dart';
import 'package:samer_client_flutter/src/models/list_response.dart';
import 'package:samer_client_flutter/src/models/monto_entity.dart';
import 'package:samer_client_flutter/src/models/order_entity.dart';
import 'package:samer_client_flutter/src/models/producto_envio_pago.dart';
import 'package:samer_client_flutter/src/models/status_payment_entity.dart';
import 'package:samer_client_flutter/src/models/sub_category_entity.dart';
import 'package:samer_client_flutter/src/models/trailer_model.dart';
import 'package:samer_client_flutter/src/resources/categories_provider.dart';
import 'package:samer_client_flutter/src/resources/direction_provider.dart';
import 'package:samer_client_flutter/src/resources/favoritos_provider.dart';
import 'package:samer_client_flutter/src/resources/historial_provider.dart';
import 'package:samer_client_flutter/src/resources/list_product_provider.dart';
import 'package:samer_client_flutter/src/resources/ofertas_provider.dart';
import 'package:samer_client_flutter/src/resources/order_detail_provider.dart';
import 'package:samer_client_flutter/src/resources/order_provider.dart';
import 'package:samer_client_flutter/src/resources/payment_provider.dart';
import 'package:samer_client_flutter/src/resources/product_detail_provider.dart';
import 'package:samer_client_flutter/src/resources/productos_principal_provider.dart';
import 'package:samer_client_flutter/src/resources/status_payment_provider.dart';
import 'package:samer_client_flutter/src/resources/sub_categories_provider.dart';
import 'google_maps_provider.dart';
import 'movie_api_provider.dart';
import '../models/item_model.dart';

class Repository {

  final moviesApiProvider = MovieApiProvider();
  final productsProvider = ProductoPrincipalApiProvider();
  final googleMapsProvider = GoogleMapsProvider();
  final directionProvider = DirectionProvider();
  final statusPaymentProvider = StatusPaymentProvider();
  final orderProvider = OrderProvider();
  final historialProvider = HistorialProvider();
  final paymentsProvider = PaymentsProvider();
  final ofertasProvider = ProductoOfertaApiProvider();
  final favoritosProvider = FavoritosApiProvider();
  final productDetailProvider = ProductoDetailApiProvider();
  final categoriesProvider = CategoriesApiProvider();
  final SubCategoriesProvider = SubCategoriesApiProvider();
  final ListProductProvider = ListProductApiProvider();
  final orderDetailProvider = OrderDetailProvider();


  Future<ItemModel> fetchAllMovies() => moviesApiProvider.fetchMovieList();

  Future<TrailerModel> fetchTrailer(int id) => moviesApiProvider.fetchTrailer(id);

  Future<ProductsResults> fetchProducts()=> productsProvider.fetchProducts();

  Future<DirectionResponseModel> fetchDirection(String latlng)=> googleMapsProvider.fetchDirection(latlng);

  Future<DirectionResults> fetchDirectionUser()=> directionProvider.fetchDirections();

  Future<StatusPaymentEntity> fetchStatusPayment()=> statusPaymentProvider.fetchStatusPayment();

  Future<OrdersResults> fetchOrders()=> orderProvider.fetchOrders();

  Future<OrdersResults> fetchHistorial()=> historialProvider.fetchHistorial();

  Future<MontoEntity> fetchPayments(List<ProductoEnvioPago> list_product,int id_place)=> paymentsProvider.fetchPayments(list_product, id_place);

  Future<ProductsResults> fetchOfertas()=> ofertasProvider.fetchOfertas();

  Future<ProductsResults> fetchFavoritos()=> favoritosProvider.fetchFavoritos();

  Future<ProductoEntity> fetchProductDetail(int pk)=> productDetailProvider.fetchProducts(pk);

  Future<CategoriesResults> fetchCategories()=> categoriesProvider.fetchCategories();

  Future<SubCategoriesResults> fetchSubCategories(int pk)=> SubCategoriesProvider.fetchSubCategories(pk);

  Future<ListResponse> fetchListProduct(int pk)=> ListProductProvider.fetchListProduct(pk);

  Future<DetalleResponseEntity> fetchOrderDetail(int pk)=> orderDetailProvider.fetchOrderDetail(pk);


}